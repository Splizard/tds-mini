function love.load()
	require("util")
	
	love.filesystem.setIdentity( "RTS mini" )
	if not love.filesystem.isDirectory("mods") then
		local ok = love.filesystem.mkdir("mods")
		if not ok then
			game.log("Could not create mods directory!", "error")
		end
	end

	config = game.loadConfig()
	
	--Fullscreen function
	love.graphics.setBackgroundColor(0,0,0)
	modes = love.window.getFullscreenModes()
	table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)   -- sort from smallest to largest
	table.getn(modes)
	if config.resolution then
		windowWidth,windowHeight = config.resolution:match("^(%d*)x(%d*)")
	else
		windowWidth = modes[table.getn(modes)].width and 800
		windowHeight = modes[table.getn(modes)].height and 600
	end
	--love.graphics.setMode(1024,768, false, true, 0)
	love.window.setMode(windowWidth,windowHeight, {
		fullscreen = config.fullscreen or false, 
		vsync = true, 
	}) 
	--set the window dimensions to 650 by 650 with no fullscreen, vsync on, and no antialiasing
	windowWidth, windowHeight = love.window.getMode( )
	
	server = nil
	client = true

	--Load mods--
	require("networking")
	require("api")
	require("units")
	require("buildings")
	require("collision")
	require("camera")
	require("menu")
	require("editor")
	require("bullets")
	require("objects")
	require("map")

	--Player stuff
	player = {}
	player[1] = {
		team = 1,
		colour = {0, 255, 0},
		keys = {},
		cache = {}
	}
	player[2] = {
		team = 2,
		colour = {255, 0, 0},
		keys = {},
		cache = {}
	}
	money = 200
	
	font = fonts("mono",14)
	
	menu.load()
	editor.load()
	
	--Load logo for main menu
	menu.logo = love.graphics.newImage("data/images/menu.png")
	
	--Enter menu
	state = "menu"
	load = "game"
	
	--Map terrain		
	sand = love.graphics.newImage("data/images/sand.png")
	sand:setWrap("repeat","repeat")
	
	quad = love.graphics.newQuad(0,0,mapWidth,mapHeight,48,48)
	
	packet = 0
	timer = 0
	
	slow = false
	
	--Splitscreen Setup.
	--TODO come up with a fancy formulae to do this automagicaly.
	--^^( Think about it 18 player splitscreen on retina screens!!)
	screens = {
		[0]={0,0,windowWidth,windowHeight},
		[1]={
			[1]={0,0,windowWidth,windowHeight}, 
			[2]={0,0,windowWidth/2-2,windowHeight},
			[3]={0,0,windowWidth,windowHeight/2-2},
			[4]={0,0,windowWidth/2-2,windowHeight/2-2}
		},
		[2]={
			[2]={windowWidth/2+2,0,windowWidth/2-2,windowHeight},
			[3]={0,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2},
			[4]={windowWidth/2-2,0,windowWidth/2-2,windowHeight/2-2},
		},
		[3]={
			[3]={windowWidth/2-2,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2},
			[4]={0,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2}
		},
		[4]={
			[4]={windowWidth/2-2,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2}
		}
	}
end

function love.keypressed(key,unicode)
	menu.keypressed(key,unicode)
	editor.keypressed(key,unicode)
end

--Timing variables.
--tickrate: the amount of ticks per second.
original_tickrate = 30
tickrate = original_tickrate
tick = 1/tickrate
ticks = 0
current_tick = 0

--updaterate: the amount of network updates per second.
updaterate = 20
update = tickrate/updaterate
updates = 0

dt = tick

function love.update(dt)

	start = os.clock()
	ticks = ticks + 1
	current_tick = current_tick + 1
	
	timer = timer + dt
	if timer > 1 then
		timer = 0
		if packet > 0 then
			--print("packet rate: "..packet.." packets per second\r")
		end
		packet = 0
		updates = 0
--		for id, unit in pairs(unitsInGame) do
--			for i,v in pairs(unit.tick) do
--				v = nil
--			end
--		end
	end

	if state == "game" or state == "load" then
		--Main Game Loop.
		world:update(dt)
		
		if server then networking.handleServer(server) end
		if udp then networking.handleClient(dt) end
			
		local status, err = pcall(unitStep,dt)
		if not status then io.write("[ERROR] "..err.."\n") end
	
		local status, err = pcall(bulletStep,dt)
		if not status then io.write("[ERROR] "..err.."\n") end

	elseif state == "editor" then
		editor.update()
	else
		menu.update()
	end
	
	if current_tick >= tickrate then
		current_tick = 0
	end
	
	benchmark = os.clock() - start
	if benchmark > tick then
		tickrate = tickrate * 0.8
		slow = true
		tick = 1/tickrate
		--game.log("Your computer is not fast enough to run the game properly.", "info")		
	elseif slow == true then
		tickrate = tickrate * 1.2
		if tickrate >= original_tickrate then
			slow = false
			--game.log("Your computer is now fast enough to run the game properly.", "info")	
		end
		tick = 1/tickrate
	end
end



function love.draw()
	local lg = love.graphics
	if state == "game" or state == "editor" then
		local v = tonumber(config.splitscreen) or 1
		for i=1, v do
			lg.push()
			lg.translate(screens[i][v][1], screens[i][v][2])
			windowWidth, windowHeight = screens[i][v][3], screens[i][v][4]
			love.graphics.setScissor(screens[i][v][1], screens[i][v][2],screens[i][v][3], screens[i][v][4])
			
			--lg.setColor( 0, 0, 0)
			--lg.rectangle("fill", 0, 0, windowWidth, windowHeight)			
			
			for id, unit in pairs(unitsInGame) do
				if id == control then
					if unit.buffer[3] and unit.buffer[3].t then
						x,y = caclulateNetPos(unit, i)
						scale = (love.timer.getTime( )-camerat)
						x = lerp(camerax, x, scale)
						y = lerp(cameray, y, scale)
						camerax = x
						cameray = y
						camerat = love.timer.getTime( )
						camera:setPosition(
							x-((windowWidth/2)*camera.scaleX)-(unit.origin[1]*camera.scaleX), 
							y-((windowHeight/2)*camera.scaleY)-(unit.origin[1]*camera.scaleX)
						)
					end		
				end
			end
			
			status, err = pcall(drawMap,lg)
			if not status then io.write("[ERROR] "..err.."\n") end
	
			camera:set()
			status, err = pcall(drawUnitsStatsBelow,lg)
				
			status, err = pcall(drawBullets,lg)
			if not status then io.write("[ERROR] "..err.."\n") end
		
			if not status then io.write("[ERROR] "..err.."\n") end		
			status, err = pcall(drawUnits,lg)
		
			if not status then io.write("[ERROR] "..err.."\n") end		
			status, err = pcall(drawObjects,lg)
		
			--if not status then io.write("[ERROR] "..err.."\n") end		
			--status, err = pcall(drawUnitsStats,lg)
			if not status then io.write("[ERROR] "..err.."\n") end
			camera:unset()
			
			love.graphics.setScissor()
			lg.pop()
			windowWidth, windowHeight = screens[0][3], screens[0][4]
		end
		
		if v == 2 then
			lg.setColor( 200, 200, 200)
			lg.rectangle("fill", windowWidth/2-2, 0, 4, windowHeight)
		elseif v == 3 then
			lg.setColor( 200, 200, 200)
			lg.rectangle("fill", 0, windowHeight/2-2, windowWidth,4)
			lg.rectangle("fill", windowWidth/2-2, windowHeight/2-2, 4, windowHeight/2-2)
		elseif v == 4 then
			lg.setColor( 200, 200, 200)
			lg.rectangle("fill", 0, windowHeight/2-2, windowWidth,4)
			lg.rectangle("fill", windowWidth/2-2, 0, 4, windowHeight)
		end
		
		--Print FPS (debugging purposes).
		lg.setFont(fonts("mono",14))
		lg.print("TDS mini 0.2", windowWidth-90, 4+0)
		lg.print("FPS: "..love.timer.getFPS(),windowWidth-90, 16+0)
	elseif state == "load" then
		lg.setFont(fonts("regular",64))
		lg.printf("Loading...", 0, windowHeight/2,windowWidth,"center")
	else
		menu.draw(lg)
	end
end

function love.quit()
	game.saveConfig(config)
	game.log("\n----------------------------\n\n","none")
end
