unitsInGame = {}     --Units

function unitClearCache(unit)
	--Identify unit.
	local uid = tonumber(string.match(unit, "%d+"))
	local unit = unitsInGame[uid]
	
	player[unit.team].cache.unsafe = true
end

function unitClearCache2(unit)
	--Identify unit.
	local uid = tonumber(string.match(unit, "%d+"))
	local unit = unitsInGame[uid]
	
	player[unit.team].cache.unsafe = false
end

--Collisions
function unitHit(unit,bullet)
	--Identify unit.
	local uid = tonumber(string.match(unit, "%d+"))
	local unit = unitsInGame[uid]

	--Identify bullet.
	local bid = tonumber(string.match(bullet, "%d+"))
	local bullet = bulletsInGame[bid]

	if client and uid ~= bullet.shooter then
		--Stop and destroy bullet.
		bullet.body:setLinearDamping(2)
		bullet.dead = true
	elseif uid ~= bullet.shooter and not bullet.dead then
		if unit.team == 0 or (unit.team ~= bullet.team) then

			--Damage shield
			if unit.shield > 0 then
				unit.shield = unit.shield - (bullet.damage * (unit.resistance[bullet.type] or 1))

			--Damage armour
			elseif unitsInGame[uid].armour > 0 then
				unit.armour = unit.armour - (bullet.antiArmour * (unit.resistance[bullet.type] or 1))

			--Damage health
			else unit.health = unit.health - (bullet.damage * (unit.resistance[bullet.type] or 1)) end

			if unit.shield <= 0 then
				if unit.armour > 0 then
					unit.armour = unit.armour + unit.shield
				else
					unit.health = unit.health + unit.shield
				end
				unit.shield = 0
			end

			if unit.armour <= 0 then
				unit.health = unit.health + unit.shield
				unit.armour = 0
			end


			--AI: victim attacks shooter
			if not unit.target and bullet.shooter and not unit.heals then
				unit.target = bullet.shooter
				unit.targetType = "unit" --FIXME: when buildings can fire
			end
		end
		--Stop and destroy bullet.
		bullet.body:setLinearDamping(2)
		bullet.dead = true

		--Apply variable changes.
		bulletsInGame[bid] = bullet
		unitsInGame[uid] = unit
	end
end

--Unit movement.
moveUnit = function(unit,x,y)
	if math.dist(unit.body:getX(), unit.body:getY(), x, y) > 20 then

		--Turn towards direction of movement.
		rotateUnit(unit,x,y)

		--Continue when rotation is finished.
		if not unit.rotating then

			--Release unit.
			unit.body:setLinearDamping(0)

			--Get the vector.
			local vx = x - unit.body:getX()
			local vy = y - unit.body:getY()

			--Normalize the vector.
			local len = math.sqrt(vx*vx + vy*vy)
			
			--Move unit.
			unit.body:setLinearVelocity( (vx / len)*unit.speed, (vy / len)*unit.speed )
		else
			--Stop movement.
			unit.body:setLinearDamping(2)
		end
		unit.moving = true

	else
		--Lock unit's movement.
		unit.body:setLinearDamping(2)
		unit.moving = false
	end
end

--Unit rotation.
rotateUnit = function(unit,x,y)

	--Get angle to posistion.
	local angle = math.angle(unit.body:getX(),unit.body:getY(),x,y)

	--Check if unit has reached target angle.
	if not (unit.rotate < angle + 0.05 and unit.rotate > angle - 0.05) then

		--Reset rotation on every 360 turn
		if unit.rotate > math.pi then
			unit.rotate = -math.pi
		elseif unit.rotate < -math.pi then
			unit.rotate = math.pi
		end

		--Calculate direction to turn.
		local dir = rotateClosest(unit.rotate, angle)

		--Rotate unit.
		if dir == "CCW" then
			unit.rotate = unit.rotate - (unit.rev * dt)
		elseif dir == "CW" then
			unit.rotate = unit.rotate + (unit.rev * dt)
		end
		unit.rotating = true

	else
		unit.rotating = false
	end
end

--AI: Determine closest enemy unit.
findClosestEnemy = function(unit,i)

	--Define variables.
	local closest
	local targetType
	local range
	local x = unit.body:getX()
	local y = unit.body:getY()

	--Check if unit has LOS attribute.
	if unit.los > 0 then
		range = unit.los
	else
		range = weapons[unit.weapon].range
	end

	--Loop through units.
	for id, enemy in pairs(unitsInGame) do
		if id ~= i and teamIsHostile(enemy.team,unit.team) and (not enemy.dead) then
			local close = love.physics.getDistance( unit.fixture, enemy.fixture )
			if (closest or range) > close then
				unit.target = id
				closest = close
				targetType = "unit"
			end
		end
	end

	unit.targetType = targetType
end

--Unit Combat
unitShoot = function(unit,id,x,y,rotate,dt)

	
	
	--Rotate unit towards target.
	if rotate then rotateUnit(unit,x,y,dt) end

	--Continue when rotation is finished.
	if not unit.rotating then

		--Identify weapon
		local weapon = weapons[unit.weapon]

		--Ranged weapons.
		if weapon.mode == "Ranged" then
			if (unit.maxAmmo == 0 or unit.ammo > 0) and unit.clip > 0 then
				if unit.timer > weapon.delay and (unit.burst < weapon.burst or weapon.burst == 0) then
					local unitX = unit.body:getX()
					local unitY = unit.body:getY()
					local angle = math.angle(unitX,unitY,x,y)

					--Multiple projectiles loop.
					for i=0, table.getn(weapon.origin), 2 do
						if weapon.origin[i-1] and weapon.origin[i] then

							local weaponX = weapon.origin[i-1]
							local weaponY = weapon.origin[i]
							local vx = weaponX*math.cos(-angle) + weaponY*math.sin(-angle)
							local vy = weaponY*math.cos(-angle) - weaponX*math.sin(-angle)

							--Create the projectile.
							RTS.spawn_bullet(weapon.id, unitX+vx, unitY+vy, x+vx, y+vy, unit.team,id)
						end
					end

					--Projectile fire Sound.
					if sounds[unit.weapon][1] ~= nil then
						if not sounds[unit.weapon][1]:isStopped() then sounds[unit.weapon][1]:stop() end
						love.audio.play(sounds[unit.weapon][1])
					end

					--Manage clip.
					if unit.clipSize > 0 then
						unit.clip = unit.clip - 1
						unit.burst = unit.burst + 1
						if unit.clip == 0 then unit.reloadSound = true end
					end

					--Manage ammo.
					if unit.maxAmmo then
						unit.ammo = unit.ammo - 1
					end

					--Manage burst.
					if unit.burst == weapons[unit.weapon].burst  then
						unit.burstTimer = 0
					end

					unit.timer = 0
				end
				--Reset burst timer.
				if unit.burstTimer > weapon.burstDelay and unit.burst == weapon.burst then
					unit.burst = 0
				end
			end

		--Melee weapons.
		elseif weapon.mode == "Melee" then
			if unit.timer > weapon.delay then
				--TODO: Create sensor melee system.
				if not client then
					unitsInGame[unit.target].health = unitsInGame[unit.target].health - weapon.damage
				end
				--Projectile fire Sound.
				if sounds[unit.weapon][1] ~= nil then
					if not sounds[unit.weapon][1]:isStopped() then sounds[unit.weapon][1]:stop() end
					love.audio.play(sounds[unit.weapon][1])
				end
				unit.timer = 0
			end
		end
	end
end

--Reload unit's weapon.
unitReload = function(unit)
	unit.clip = 0

	--Play reload sound
	if unit.timer >= unit.reloadTime/3 then
		if sounds[unit.weapon][2] ~= nil and unit.reloadSound == true then
			if not sounds[unit.weapon][2]:isStopped() then sounds[unit.weapon][2]:stop() end
			love.audio.play(sounds[unit.weapon][2])
			unit.reloadSound = false
		elseif unit.reloadSound == true then
			unit.reloadSound = false
		end
	end

	--Reload.
	if unit.timer >= unit.reloadTime then
		unit.clip = unit.clipSize
		unit.reload = false
		unit.reloadSound = false
	end
end

--Kill unit and cleanup stuff.
destroyUnit = function(unit,id)
	if not unitsInGame[id].dead then
		--Remove unit.
		unitsInGame[id].dead = true
		unitsInGame[id].body:setActive(false)
		--table.remove(unitsInGame,id)

		--Stop units's reloading sound if the unit is dead.
		if unit.reload == true and sounds[unit.weapon] and sounds[unit.weapon][2] then sounds[unit.weapon][2]:stop() end

		--Cleanup loose ends.
		for eid, enemy in pairs(unitsInGame) do

			--Remove targets to this unit.
			if enemy.target == id then
				enemy.target = nil
			end

			--Shift targets down.
	--		if enemy.target ~= nil and enemy.target > id and enemy.targetType == "unit" then
	--			enemy.target = enemy.target - 1
	--		end

			--Remove waypoint targets to this unit.
			if enemy.waypoint ~= nil and enemy.waypoint[enemy.flag] ~= nil and enemy.waypoint[enemy.flag].target == id and enemy.waypoint[enemy.flag].targetType == "unit" then
				enemy.waypoint[enemy.flag].target = nil
				enemy.flag = enemy.flag + 1
			end

			--Shift waypoint targets down.
	--		if enemy.waypoint ~= nil then
	--			for wid,waypoint in pairs(enemy.waypoint) do
	--				if waypoint.target ~= nil and waypoint.target > id and waypoint.targetType == "unit" then
	--					waypoint.target = waypoint.target - 1
	--				end
	--			end
	--		end
		
		end
	
		--Sort out alpha units and HUD stuff.
		if alpha == id and alphaType == "unit" then alpha = nil end
		if alpha and id < alpha and alphaType == "unit" then alpha = alpha - 1 end
	
		for bid, bullet in pairs(bulletsInGame) do

			--Remove shooter from this bullet.
			if bullet.shooter == id then bullet.shooter = nil end

			--Shift shooters down.
			--if bullet.shooter ~= nil and bullet.shooter > id then bullet.shooter = bullet.shooter-1 end
		end
	end
end

commandUnit = function(unit,id,dt)
	local x,y
	--Move forwards.
	if unit.keys["w"] then
		x, y = unit.body:getWorldVector( 50, 0 )
		unit.moving = true
	--Move backwards.
	elseif unit.keys["s"] then
		x, y = unit.body:getWorldVector( -50, 0 )
		unit.moving = true
	else
		--Lock unit's movement.
		unit.body:setLinearDamping(2)
		unit.moving = false
	end

	if unit.moving == true then
		unit.waypoint = nil

		--Release unit.
		unit.body:setLinearDamping(0)

		--Get the vector.
		local vx = x
		local vy = y

		--Normalize the vector.
		local len = math.sqrt(vx*vx + vy*vy)
		
		if unit.realX and unit.realY then
			rx, ry = unit.body:getX()/unit.realX, unit.body:getY()/unit.realY
			unit.realX, unit.realY = nil, nil
			--print(rx, ry)
		else
			rx, ry = 1, 1
		end
		
		if client then
		unit.body:setLinearVelocity(unit.vx,unit.vy)
		else
		--Move unit.
		unit.body:setLinearVelocity(((vx / len)*unit.speed)*rx, ((vy / len)*unit.speed)*ry)
		end
	else
		unit.body:setLinearDamping(100)
	end

	--Rotate right.
	if unit.keys["d"] then
		unit.rotate = unit.rotate + (unit.rev * dt)
	--Rotate left.
	elseif unit.keys["a"] then
		unit.rotate = unit.rotate - (unit.rev * dt)
	end
	if unit.rotate > math.pi then
		unit.rotate = -math.pi
	elseif unit.rotate < -math.pi then
		unit.rotate = math.pi
	end
	

	--Shoot.
	if unit.keys["_"] then
		if client then error() end
		if (unit.maxAmmo == 0 or unit.ammo > 0) and unit.clip > 0 then
			--local cx, cy, cr = unit.body:getX(), unit.body:getY(), unit.rotate
			--local t = longertimer - (1/updaterate)
			--unit.body:setX(lerp(cx, unit.buffer.x, t)) unit.body:setY(lerp(cy, unit.buffer.y, t)) unit.rotate = unit.buffer.r
			--unit.body:setAngle(unit.rotate)
			local x, y = unit.body:getWorldPoint( 50, 0 )
			unitShoot(unit,id,x,y,false,dt)
			--unit.body:setX(cx) unit.body:setY(cy) unit.rotate = cr
			--unit.body:setAngle(unit.rotate)
		end
	end
end

scaleTo = function(x)
	--Scale building to fit
	local scaleX = 1
	if x > 1 then scaleX = 1/x end
	if x > 1 then scaleX = 1/x end
	return scale
end


function lerp(v0, v1, t)
	--r = cache.rotate + (unit.rev*((os.clock()-cache2.t)))
  return v0+((v1-v0)*t)
end

function normalise(max)
	
end

--TODO lerp between now and where its supposed to be in t time.

--Calculates Interpolation of unit.
caclulateNetPos = function(unit, i)
	local cache = unit.buffer[4]
	local cache2 = unit.buffer[1] or unit.buffer[2] or unit.buffer[3]
	assert(cache2.x)
	
	if not unit.buffer[4].t then unit.buffer[4].t = love.timer.getTime( ) end
	if not unit.buffer[4].x then unit.buffer[4].x = unit.body:getX() end
	if not unit.buffer[4].y then unit.buffer[4].y = unit.body:getY() end
	if not unit.buffer[4].rotate then unit.buffer[4].rotate = unit.rotate end
	
	--print(((os.clock()-cache2.t)))
	--print(os.clock()-cache2.t == 0)
	--print(cache2.t-cache.t)
	scale = (love.timer.getTime( )-cache2.t)
			--print(scale)
--	if scale < 0 then
--		scale = ((love.timer.getTime( )-unit.buffer[4].t)-((cache2.t-unit.buffer[4].t))*2) / (cache2.t-unit.buffer[4].t)
--			--print(scale)
--	
--		x = unit.buffer[4].x
--		y = unit.buffer[4].y
--	else

		--print((os.clock()-cache2.t)*scale)
		x = lerp(cache.x, cache2.x, scale )
		y = lerp(cache.y, cache2.y, scale )
	--print(x)
	--end
	
	return x, y, cache, cache2
end



unitStep = function(dt)

	--Centre on unit if spacebar is down.
	if alpha and alphaType == "unit" and love.keyboard.isDown(" ") then
		local unit = unitsInGame[alpha]
		camera:setPosition(unit.body:getX()-(windowWidth/2)-unit.origin[1], unit.body:getY()-(windowHeight/2)-unit.origin[1])
	end

	for id, unit in pairs(unitsInGame) do
		if not unitsInGame[id].dead then
			--Destroy unit if it has no health or if it has been deleted.
			if unit.health < 1 then
				destroyUnit (unit,id)
			else
				local default = units[unit.id]

				--Update selftimers.
				unit.timer = unit.timer + dt
				unit.burstTimer = unit.burstTimer + dt
				unit.shieldTimer = unit.shieldTimer + dt

				--Stop unit health from overfilling.
				if unit.health > default.health then
					unit.health = default.health
				end

				--Stop unit shield from overfilling.
				if unit.shield > default.shield then
					unit.shield = default.shield
				end

				--Recharge shield TODO: Create better recharge system.
				if unit.shield < default.shield then
					unit.shield = unit.shield + 0.01
					unit.shieldTimer = 0
				end

				--Reload weapon when it is at the end of its clip.
				if unit.clip < 1 or (unit.selected == true or unit.command == true) and love.keyboard.isDown("r") then
					unitReload(unit)
				end

				--Rotate units Box2d body.
				if unit.body:getAngle() ~= unit.rotate then
					unit.body:setAngle(unit.rotate)
				end

				--Commander mode.
				--if unit.team == (player[control].team) then
					commandUnit(unit,id,dt)
				
				--end
				
				if client then
					if id == control then
						if state == "load" and load == "game" then state = load end	
					end
				elseif unit.ai then

					--Move towards next waypoint.
					--[[if unit.waypoint ~= nil and unit.waypoint[unit.flag] ~= nil and unit.waypoint[unit.flag].target == nil then
							moveUnit(unit, unit.waypoint[unit.flag].x, unit.waypoint[unit.flag].y)

							if unit.moving == false then
								if unit.waypoint[unit.flag+1] == nil then
									unit.waypoint = nil
									unit.flag = 1
								else
									unit.waypoint[unit.flag] = nil
									unit.flag = unit.flag +1
								end
							end--]]

					--If idle start attack step.
					if unit.weapon ~= "Unarmed" then
						unit.body:setLinearDamping(2)

						

						--Find closest enemy.
						if not unit.target or not unitsInGame[unit.target] or unit.target == id then
							findClosestEnemy(unit,id)

						elseif unit.target then

							local Ex = unitsInGame[unit.target].body:getX()
							local Ey = unitsInGame[unit.target].body:getY()
							local Ef = unitsInGame[unit.target].fixture

							if unit.los > 0 and love.physics.getDistance( unit.fixture, Ef ) > unit.los then
								unit.target = nil
							else
								--Move towards if out of range
								if (unit.maxAmmo == 0 or unit.ammo > 0 ) and love.physics.getDistance( unit.fixture, Ef ) > weapons[unit.weapon].range then
									moveUnit(unit,Ex,Ey)

								--Move away if within minimum distance
								elseif (unit.maxAmmo == 0 or unit.ammo > 0 ) and love.physics.getDistance( unit.fixture, Ef ) < weapons[unit.weapon].minRange then
									moveUnit(unit,-Ex,-Ey)

								elseif (unit.clipSize == 0 or unit.clip > 0) and (unit.maxAmmo == 0 or unit.ammo > 0 ) then
									unit.body:setLinearDamping(2)
									unitShoot (unit,id,Ex,Ey,true,dt)
									unit.shooting = true
								end
							end
						
						end
					end
				end
			end
		else
			if not unit.respawning then
				unit.respawning = true
				game.after(2, RTS.respawn_unit, unit)
			end
		end
	end

	--UNIT Cleanup--
--	for id,unit in pairs(unitsInGame) do
--		if unit.fixture:getUserData() ~= "unit "..id then
--			unit.fixture:setUserData("unit "..id)
--		end
--	end
end

local function wrap(v,l,u)
	rz = u-l
	if (v>=l) and (v<=u) then
		return v
	end
	
	return (v % rz)+l
end

local function slerp(a, b, t)
	a, b = math.deg(a),math.deg(b)
	d = math.abs(b-a)
	if (d > 180) then
		if b > a then
			a = a + 360
		else
			b = b + 360
		end
	end
	
	v = (a+((b-a)*t))
	
	rz = 360
	if (v >= 0) and (v <= 360) then
		return math.rad(v)
	end
    return math.rad(v % rz)
end

--Check if unit is within camera.
local function unitInBounds(x,y,unit)
	if (x+(unit.origin[1]*camera.scaleX)) < camera.x or 
	(x-(unit.origin[1]*camera.scaleX)) > (camera.x+(windowWidth*camera.scaleX)) then
		return false
	end
	if (y+(unit.origin[2]*camera.scaleY)) < camera.y or 
	(y-(unit.origin[2]*camera.scaleY)) > (camera.y+(windowHeight*camera.scaleY)) then
		return false
	end
	return true
end

--Draw units on map.
function drawUnits(lg)
	local benchmark = os.clock() - start

	for id, unit in pairs(unitsInGame) do
		
		if (not unitsInGame[id].dead) then
		
			local x = unit.body:getX()
			local y = unit.body:getY()
			local r = unit.rotate
		
			--print(x,y)
		
			--for i=3,1,-1 do
				local i = 2
				if unit.buffer[i] and unit.buffer[i].t then
					x,y, cache, cache2 = caclulateNetPos(unit, i)
					scale = (love.timer.getTime( )-cache2.t)
					if cache.rotate ~= cache2.rotate then
						r = slerp(cache.rotate, cache2.rotate, scale)
						--print(r)
						--print(r)
						--local dir = rotateClosest(cache.rotate, cache2.rotate)
						if dir == "CW" then
							--r = cache.rotate + unit.rev*scale
						else
							--r = cache.rotate - unit.rev*scale
						end
					else
						r = cache.rotate
					end
					--print(x,y)
					--break
					--print((os.clock()-lastupdate[i]))
					cache.rotate = r
					cache.x = x
					cache.y = y
					cache.t = love.timer.getTime( )
				end
				--print(r)
			--end
			
			--Make sure unit is within camera.
		 	if unitInBounds(x,y,unit) then
		
				--camera:setPosition(x-(windowWidth/2)-unit.origin[1], y-(windowHeight/2)-unit.origin[1])
	
				--Draw gun.
				if images[unit.id] and images[unit.id]["gun"] then
					lg.draw(images[unit.id].gun,x,y,r,1,1,unit.origin[1],unit.origin[2])
				else
					lg.draw(images[unit.race].gun,x,y,r,1,1,unit.origin[1],unit.origin[2])
				end
				if images[unit.weapon][2] ~= nil then
					lg.draw(images[unit.weapon][2],x,y,r,1,1,unit.origin[1],unit.origin[2])
				end
		
				--Set team color.
				--lg.setColorMode("modulate")
				--if unit.team == player[control].team then
					lg.setColor( 0, 255, 0)
				--else
					lg.setColor( 255, 0, 0)
				--end
		
				--Draw body.
				if images[unit.id] and images[unit.id]["body"] then
					lg.draw(images[unit.id].body,x,y,r,1,1,unit.origin[1],unit.origin[2])
				else
					lg.draw(images[unit.race].body,x,y,r,1,1,unit.origin[1],unit.origin[2])
				end
				lg.setColor(255,255,255)
		
				--Draw Head.
				if images[unit.id] and images[unit.id]["head"] then
					lg.draw(images[unit.id].head,x,y,r,1,1,unit.origin[1],unit.origin[2])
				else
					lg.draw(images[unit.race].head,x,y,r,1,1,unit.origin[1],unit.origin[2])
				end
			end
		end
	end
	start = os.clock()
end

function drawUnitsStatsBelow(lg)	
	for id, unit in pairs(unitsInGame) do
		if not unitsInGame[id].dead then
			if unit.selected == true then
				--Selection circle
				if unit.team == player[control].team then
					lg.setColor( 0, 255, 0)
				else
					lg.setColor( 255, 0, 0)
				end
				lg.circle("line", unit.body:getX(), unit.body:getY(), unit.selectionCircle)
			elseif display_team_colours then
				--Team circle
				if unit.team == player[control].team then
					lg.setColor( 0, 255, 0,50)
				else
					lg.setColor( 255, 0, 0,50)
				end
				lg.circle("line", unit.body:getX(), unit.body:getY(), unit.selectionCircle)
			end
		end
	end
end

--Draw selection circles, health bars etc..
function drawUnitsStats(lg)
	for id, unit in pairs(unitsInGame) do
		if not unitsInGame[id].dead then
			--if unit.selected == true then
				bar = true

				--Draw Waypoints
				if unit.waypoint ~= nil and unitsInGame[id].team == player[control].team  then
					lg.setColor( 0, 255, 0)
					--lg.setColorMode("modulate")
					for id,waypoint in pairs(unit.waypoint) do
						if waypoint.target == nil and waypoint.x ~= nil then lg.draw(waypointInGame.image,waypoint.x, waypoint.y, 0,1,1,20,48) end
					end
					lg.setColor(255,255,255)
				end

				--Commander icon
				lg.setColor( 255, 0, 0)
				if unit.commander then 
					lg.circle("fill",
						--[[x=]] unit.body:getX() - races[units[unit.id].race].selectionCircle-10,
						--[[y=]] unit.body:getY() - unit.box[2]-10, 
						--[[Radius=]] 5
					) 
				end

				--Shield bar
				lg.setColor( 0, 0, 255)
				if unit.shield > 0 then 
					lg.rectangle("fill",
						--[[x=]] unit.body:getX() - races[units[unit.id].race].selectionCircle, 
						--[[y=]] unit.body:getY() - unit.box[2]-30, 
						--[[width=]] races[units[unit.id].race].selectionCircle*2/units[unit.id].shield*(unit.shield), 
						--[[height=]] 7
					) 
				end

				--Health bar
				lg.setColor( 0, 255, 0)
				if unit.health > 0 then 
					lg.rectangle("fill",
						--[[x=]] unit.body:getX() - races[units[unit.id].race].selectionCircle, 
						--[[y=]] unit.body:getY() - unit.box[2]-20, 
						--[[width=]] races[units[unit.id].race].selectionCircle*2/units[unit.id].health*(unit.health), 
						--[[height=]] 7
					) 
				end

				--Ammo bar
				lg.setColor( 255, 255, 0)
				if unit.clip > 0 then 
					lg.rectangle("fill",
						--[[x=]] unit.body:getX() - races[units[unit.id].race].selectionCircle, 
						--[[y=]] unit.body:getY() - unit.box[2]-10, 
						--[[width=]] races[units[unit.id].race].selectionCircle*2/unit.clipSize*(unit.clip), 
						--[[height=]] 7
					) 
				else
					lg.rectangle("fill",
						--[[x=]] unit.body:getX() - races[units[unit.id].race].selectionCircle, 
						--[[y=]] unit.body:getY() - unit.box[2]-10, 
						--[[width=]] races[units[unit.id].race].selectionCircle*2/unit.reloadTime*(unit.timer), 
						--[[height=]] 7
					)
				end
			end
		--end
	end
end

--Draw units on minimap.
function drawUnitsMinimap(lg)
	for id, unit in pairs(unitsInGame) do
		if not unitsInGame[id].dead then
			if unit.team == player[control].team then
				if unit.selected == true then
					lg.setColor( 255, 255, 255)
				else
					lg.setColor( 0, 255, 0)
				end
			else
				lg.setColor( 255, 0, 0)
			end
			if unit.class == "unit" then
				lg.circle("fill",
					--[[x=]] ( ( ( unit.body:getX() - unit.origin[1] ) / mapWidth ) * miniMapWidth ) + miniMapX + 0,
					--[[y=]] ( ( ( unit.body:getY() - unit.origin[1] ) / mapHeight ) * miniMapHeight ) + miniMapY + 0,
					--[[radius=]] ( ( unit.box[2]*2 / mapHeight ) * miniMapHeight )
				)
			elseif unit.class == "vehicle" then
				lg.rectangle("fill",
						--[[x=]] ( ( ( unit.body:getX() - unit.origin[1] ) / mapWidth ) * miniMapWidth ) + miniMapX + 0,
						--[[y=]] ( ( ( unit.body:getY() - unit.origin[1] ) / mapHeight ) * miniMapHeight ) + miniMapY + 0,
						--[[width=]] ( ( unit.box[2] / mapHeight ) * miniMapHeight),
						--[[height=]] ( ( unit.box[3] / mapHeight ) * miniMapHeight)
					)
			end
		end
	end
end

function drawUnitsHud(lg)
	local ammo
	local unit = unitsInGame[alpha]
	
	local scaleX = 1
	local scaleY = 1
	if unit.box[2] > 100 then scaleX = 100/unit.box[2] end
	if unit.box[3] > 100 then scaleY = 100/unit.box[3] end
	if scaleX < scaleY then scale = scaleX else scale = scaleY end

	if unit.maxAmmo == 0 then ammo = "∞" else ammo = tostring(unit.ammo) end

	if bar == true then 	--We dont want this text to be visible when HUD is hiding.
		lg.print(unit.name, 2-100+barPos, 4+0)  --Unit name.
		lg.print(unit.race, 2-100+barPos, 16+0) --Unit race.
	end

	--Unit Health, Ammo and Clip.
	lg.print("Health: "..math.round(unit.health), (barPos - 100)+2, 134)
	lg.print("Armour: "..math.round(unit.armour), (barPos - 100)+2, 146)
	lg.print("  Ammo: "..ammo, (barPos - 100)+2, 158)
	lg.print("  Clip: "..(unit.clip), (barPos - 100)+2, 170)

	--Debug placeholder
	--lg.print("Debug: "..var, 21+0-100+barPos, 170+0)
	--lg.print("Debug: "..var, 21+0-100+barPos, 182+0)

	--Draw unit on HUD.
	if images[unit.weapon][2] then
		--Units weapon.
		lg.draw(images[unit.weapon][2], (barPos - 100) + 50, 80, unit.rotate, scale, scale, unit.origin[1], unit.origin[2])
	end

	--Units gun.
	if images[unit.id] and images[unit.id]["gun"] then
		lg.draw(images[unit.id].gun, (barPos - 100) + 50, 80, unit.rotate, scale, scale, unit.origin[1], unit.origin[2]) --Units "gun"
	else
		lg.draw(images[unit.race].gun, (barPos - 100) + 50, 80, unit.rotate, scale, scale, unit.origin[1], unit.origin[2]) --Races default "gun"
	end

	--Set team color for units body.
	--lg.setColorMode("modulate")
	if unit.team == player[control].team then
		lg.setColor( 0, 255, 0)
	else
		lg.setColor( 255, 0, 0)
	end
	if images[unit.id] and images[unit.id]["body"] then
		lg.draw(images[unit.id].body,50-100+barPos, 80, unit.rotate,scale,scale,unit.origin[1],unit.origin[2]) --Units "body"
	else
		lg.draw(images[unit.race].body,50-100+barPos, 80, unit.rotate,scale,scale,unit.origin[1],unit.origin[2]) --Races default "body"
	end
	lg.setColor(255,255,255)

	--Units Head.
	if images[unit.id] and images[unit.id]["head"] then
		lg.draw(images[unit.id].head,50-100+barPos, 80, unit.rotate,scale,scale,unit.origin[1],unit.origin[2]) --Units "head"
	else
		lg.draw(images[unit.race].head,50-100+barPos, 80, unit.rotate,scale,scale,unit.origin[1],unit.origin[2]) --Races default "head"
	end
end
