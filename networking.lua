local socket = require "socket"

networking = {}

--Catch command and parameters from string.
--eg. networking.parse("cmd flag1 flag2") returns: "cmd", {"flag1", "flag2"}
function networking.parse(data)
	local parms = {}
	local cmd, flags = data:match("^(%S*) (.*)")
	if flags then
		repeat
			v, p = flags:match("^(%S*) (.*)")
			if p then
				flags = p
			end
			if v then
				table.insert(parms,v)
			else
				v = flags:match("^(%S*)")
				table.insert(parms,v)
				break
			end
		until false
	else
		cmd = data
	end
	return cmd, parms	
end

--Connects to a udp server and returns connection.
--eg. networking.connect("192.168.1.71", 30001) returns: udp, error
function networking.connect(address, port) 
	udp = socket.udp()
	udp:settimeout(0)
	_, err = udp:setpeername(address, port)
	
	return udp, err
end


--Catches current keystate and returns server parsable cmd.
--eg keyState("a") returns: "a\n" or ""! a\n"
local function keyState(key)
	if love.keyboard.isDown(key) then
		--TODO: use an on keypress event for space bar.
		if key == " " then key = "_" end
		
		return (key.."\n")
	else
		--TODO: use an on keypress event for space bar.
		if key == " " then key = "_" end
		
		return ("! "..key.."\n")
	end
	return ""
end

--This function directly handles all sending and recieving of network commands in TDS mini.
local recieve_time_tracker = 0
function networking.handleClient(dt)
	local text = ""
	--Sends information to server.	
	if ticks > update then
	
		--Send key state changes
		text = text..keyState("w")
		text = text..keyState("a")
		text = text..keyState("s")
		text = text..keyState("d")
		text = text..keyState(" ")
	end
	
	--Localise some variables for speed.
	local split = split
	local tonumber = tonumber

	--Networking receive and parse loop.
	repeat
		data = udp:receive()
		
		--Some helper variables
		local gettingmap = false
		
		--Update received packets or break loop if there is no more data.
		if data then packet = packet + 1 else break end
		
		--print(data)

		--Catch update number and if it is lower then the current update, ignore it.
		local num = data:match("^(%d*)")
		if num ~= "0" then 
			assert(tonumber(num), "The server you tried to connect to is corrupted.")
		end
		if num == "0" or recieve_time_tracker <= tonumber(num) then
			recieve_time_tracker = tonumber(num)
			
			--Break packet into lines.
			for i,data in ipairs(split(data,"\n")) do
				data = string.gsub(data, "\n", "") --Strip newline character.
				--print(data)
				--Parse line.
				cmd, parms = networking.parse(data)
				if cmd then
					if cmd == "mk" then --mk name team x y health rotation
						assert(tonumber(parms[2]) and tonumber(parms[3]) and tonumber(parms[4]) and tonumber(parms[5]), "The server you tried to connect to is corrupted.")
						
						--If the player does not exist create him/her.
						--[[if not player[tonumber(parms[2])] then 
							player[tonumber(parms[2])] = {
								team=tonumber(parms[2]),
								colour = {0, 255, 0},
								keys={},
								cache = {}
							} 
						end]]--

						--Spawn new unit.
						RTS.spawn_unit(parms[1],tonumber(parms[2]),tonumber(parms[3]),tonumber(parms[4]),tonumber(parms[5]))

					elseif cmd == "id"  then
						assert(tonumber(parms[1]), "The server you tried to connect to is corrupted.")
						--Update player control.
						control = tonumber(parms[1])
						
					elseif cmd:match("u%d*")  then --u i name x y rotation team
					
							local dead = parms[2]
							if dead ~= "d" then
								local i = tonumber(parms[1])
								assert(tonumber(parms[1]) and tonumber(parms[3]) and tonumber(parms[4]) and tonumber(parms[5]),
							"The server you tried to connect to is corrupted.")
							
								--Spawn the unit if it does not exist.
								if not unitsInGame[i] then
									RTS.spawn_unit(parms[2],tonumber(parms[6]),tonumber(parms[3]),tonumber(parms[4]),100,tonumber(parms[5]), {}, i)
								end
							
								--Update 
								--unitsInGame[i].buffer[4] = {}
								unitsInGame[i].buffer[3] = unitsInGame[i].buffer[2]
								unitsInGame[i].buffer[2] = unitsInGame[i].buffer[1]
								unitsInGame[i].buffer[1] = unitsInGame[i].buffer[0]
								unitsInGame[i].buffer[0] = {}
								unitsInGame[i].buffer[0].t = love.timer.getTime( )
								unitsInGame[i].buffer[0].x = tonumber(parms[3])
								unitsInGame[i].buffer[0].y = tonumber(parms[4])
								unitsInGame[i].buffer[0].rotate = tonumber(parms[5]) or 0
								
																
								if unitsInGame[i].dead then
									unitsInGame[i].body:setActive(true)
									unitsInGame[i].dead = false
									unitsInGame[i].buffer[4] = unitsInGame[i].buffer[0]
								end
							
								--Update unit.
								unitsInGame[i].body:setX( tonumber(parms[3]) )
								unitsInGame[i].body:setY( tonumber(parms[4]) )
								unitsInGame[i].rotate = tonumber(parms[5]) or 0
								unitsInGame[i].health = tonumber(parms[7])
								
								--If the player does not exist create him/her.
--								if not player[tonumber(parms[6])] then 
--									player[tonumber(parms[6])] = {
--										team=tonumber(parms[6]),
--										colour = {0, 255, 0},
--										keys={},
--										cache = {}
--									} 
--								end
							else
								local i = tonumber(parms[1])
								destroyUnit(unitsInGame[i],i)
							end
					
					elseif cmd == "sh" then --sh id x y
						--Shoot bullet.
						assert(tonumber(parms[1]) ,"The server you tried to connect to is corrupted.")
						local id = tonumber(parms[1])
						local unit = unitsInGame[id]

						x,y = unit.body:getWorldPoint( 50, 0 )
						unitShoot(unitsInGame[id],id,x,y,false,dt)
					elseif cmd == "map" then --map
						if gotmap == nil then
							gettingmap = true
						end
					elseif cmd == "o" then --o id x y
						assert(tonumber(parms[2]) and tonumber(parms[3]))
						if gettingmap then
							RTS.spawn_object(parms[1],tonumber(parms[2]),tonumber(parms[3]))
						end
						text = text .. "ack map\n"
					elseif not cmd:match("^(%d*)") then
						print("Client: unknown command: "..cmd)
					end
				end
			end
		end
		if msg and msg ~= 'timeout' then
	        error("Network error: "..tostring(msg))
	    end
	    if gettingmap then
	    	gotmap = true
	    end
	until not data
	
	if ticks > update then
		udp:send(text)
		
		ticks = 0
	end
end

--Starts a TDS mini server on another thread.
function networking.host(port)
	--TODO pass port to server thread.
	local server = love.thread.newThread("server.lua" )
	return server
end

--Checks if server has stopped and logs error.
function networking.handleServer(server)
		err = server:getError()
		if err then print("[SERVER] "..err) game.log(err,"error") end
end

