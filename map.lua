--Initialise world		
love.physics.setMeter(64)
world = love.physics.newWorld(0, 0, true)
world:setCallbacks(beginContact, endContact)

--DEV--
--Spawn some default units
RTS.spawn_bot('commander',0,100,100)

--Teams
team = {}
team[1] = {
	hostile = {[2]=true},
	friendly = {},
}
team[2] = {
	hostile = {[1]=true},
	friendly = {},
}
teamIsHostile = function(one,two)
	if team[one].hostile[two] then
		return true
	else
		return false
	end
end
teamIsFriendly = function(one,two)
	if team[one].friendly[two] then
		return true
	else
		return false
	end
end

--Map properties
mapWidth = 100*10
mapHeight = 100*10

--Map bounding box
border = {}
border.top = {}
border.top.body = love.physics.newBody(world, mapWidth/2, 0)
border.top.shape = love.physics.newRectangleShape(mapWidth,1)
border.top.fixture = love.physics.newFixture(border.top.body, border.top.shape)
border.left = {}
border.left.body = love.physics.newBody(world, 0, mapHeight/2)
border.left.shape = love.physics.newRectangleShape(1,mapHeight)
border.left.fixture = love.physics.newFixture(border.left.body, border.left.shape)
border.bottom = {}
border.bottom.body = love.physics.newBody(world, mapWidth/2, mapHeight)
border.bottom.shape = love.physics.newRectangleShape(mapWidth,1)
border.bottom.fixture = love.physics.newFixture(border.bottom.body, border.bottom.shape)
border.right = {}
border.right.body = love.physics.newBody(world, mapWidth, mapHeight/2)
border.right.shape = love.physics.newRectangleShape(1,mapHeight)
border.right.fixture = love.physics.newFixture(border.right.body, border.right.shape)

function drawMap(lg)
	--Set to a nice green.
	lg.setColor( 75, 145, 35)
	
	local x, y = 0,0
	local w, h = windowWidth, windowHeight
	
	--Only draw the map which is visible by camera
	if camera.x < 0 then
		x = math.abs(camera.x)
	end
	if camera.y < 0 then
		y = math.abs(camera.y)
	end
	if (camera.x+windowWidth) > mapWidth then
		w = windowWidth-((camera.x+windowWidth)-mapWidth)
	end
	if (camera.y+windowHeight) > mapHeight then
		h = windowHeight-((camera.y+windowHeight)-mapHeight)
	end
	--lg.rectangle("fill", x, y, w, h)
	camera:set()
	lg.draw(sand,quad,0,0)
	camera:unset()
end
