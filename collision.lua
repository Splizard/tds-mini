local function beginCollision(a,b)
	if a then
		if a:find("unit") and b and b:find("bullet") then
			unitHit(a,b)
		elseif a:find("building") and b and b:find("bullet") then
			buildingHit(a,b)
		elseif a:find("building") then
			local ID = tonumber(string.match(a, "%d+"))
			if buildingsInGame[ID].placing or buildingsInGame[ID].ghost then
				buildingsInGame[ID].safeToPlace = buildingsInGame[ID].safeToPlace + 1
			end
		elseif a:find("bullet") and b == nil then
			--Hits a wall or object
			local ID = tonumber(string.match(a, "%d+"))
			bulletsInGame[ID].dead = true
			bulletsInGame[ID].body:setLinearDamping(2)
		elseif a:find("sensor") then
			--Identify sensor.
			local oid = tonumber(string.match(a, "%d+"))
			local object = objectsInGame[oid]

			object.free = false
		end
	end
end

local function endCollision(a,b)
	if a then
		if a:find("building") then
			local ID = tonumber(string.match(a, "%d+"))
			if buildingsInGame[ID].placing or buildingsInGame[ID].ghost then
				buildingsInGame[ID].safeToPlace = buildingsInGame[ID].safeToPlace - 1
			end
		elseif a:find("sensor") then
		--Identify sensor.
			local oid = tonumber(string.match(a, "%d+"))
			local object = objectsInGame[oid]

			object.free = true
		end
	end
end

--On collision between two objects
function beginContact(a, b, coll)
	local a = a:getUserData()
	local b = b:getUserData()
	beginCollision(a,b)
	beginCollision(b,a)
end

--Other collision functions
function endContact(a, b, coll)
   	local a = a:getUserData()
	local b = b:getUserData()
	endCollision(a,b)
	endCollision(b,a)
	collectgarbage()
end
--~
--~ function preSolve(a, b, coll)
	--~ local a = a:getUserData()
	--~ local b = b:getUserData()
	--~ if a then
		--~ if a:find("unit") then
			--~ local id = tonumber(string.match(unit, "%d+"))
			--~ local U = unitsInGame[id]
		--~ elseif a:find("building") then
			--~ local id = tonumber(string.match(unit, "%d+"))
		--~ end
	--~ end
	--~ if b then
		--~ if b:find("unit") and a and a:find("bullet") then
			--~ unitHit(b,a)
		--~ elseif b:find("building") and a and a:find("bullet") then
			--~ buildingHit(a,b)
		--~ elseif b:find("building") then
			--~ local ID = tonumber(string.match(b, "%d+"))
			--~ if buildingsInGame[ID].placing or buildingsInGame[ID].ghost then
				--~ buildingsInGame[ID].safeToPlace = buildingsInGame[ID].safeToPlace + 1
			--~ end
		--~ elseif b:find("bullet") and a == nil then
			--~ --Hits a wall or object
			--~ local ID = tonumber(string.match(b, "%d+"))
			--~ bulletsInGame[ID].dead = true
			--~ bulletsInGame[ID].body:setLinearDamping(2)
		--~ end
	--~ end
--~ end
--~
--~ function postSolve(a, b, coll)
   --~
--~ end

-- Collision detection function.
-- Checks if a and b overlap.
-- w and h mean width and height.
function CheckCollision(ax1,ay1,aw,ah, bx1,by1,bw,bh)
  local ax2,ay2,bx2,by2 = ax1 + aw, ay1 + ah, bx1 + bw, by1 + bh
  return ax1 < bx2 and ax2 > bx1 and ay1 < by2 and ay2 > by1
end
