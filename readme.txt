TDS mini

Required Libraries:
---------------------
$ sudo apt-get install love

Running:
----------
Run this command while in the tds-mini folder:
$ love .

Playing:
----------
Walk around with 'wasd' keys. Shoot with 'spacebar'.
There are options for resolution and fullscreen.

Editor:
---------
Not very usable at the moment.
Spawn trees with the 't' key and drag them around.
Press 's' to save.

Multiplayer:
--------------
Anyone can join one of your games if they input your IP Address in the multiplayer menu. (LAN)
