RTS.new_object("spawnpoint",{
	Name = "Spawn Point",
	Type = "spawning",
	Image = "spawn.png",
	ImageOrigin = {24,24},
	Invisible = true,
	CollisionBox = {"circle", 50},
	Sensor = true,
})
