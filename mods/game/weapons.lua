--SPECIAL--

	--Flame thrower
	RTS.new_weapon( "Flamethrower", {
		Name = "Flamethrower",
		Race = "Human",
		Type = "Flame",
		Bullet = "backgrounds/bullet_flame.png",
		Sounds = {"Flamethrower_fire.ogg"},
		Damage = 0.5,
		Delay = 0,
		Speed = 4,
	})
	
	--Hands
	RTS.new_weapon( "hands", {
		Name = "Hands",
		Race = "Human",
		Type = "Melee",
		Sounds = {fire="punch.ogg"},
		Mode = "Melee",
		Damage = 0.5,
		Delay = 0.5,
		Range = 1,
	})


------------------------------------------------------------------------------Sub machine gunners(smg's)----------------------------------------------------------------------------



--Type 100(third best)--
RTS.new_weapon( "Submachinegun_type100", {
	Name = "Submachinegun_type100",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"Type100_fire.ogg"},
	Damage = 0.3,
	Delay = 0.3,
	Speed = 7,
})

--ak47(best)--
RTS.new_weapon( "Submachinegun_ak47", {
	Name = "Submachinegun_ak47",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"ak47_fire.ogg"},
	Damage = 1,
	Delay = 0.5,
	Speed = 7,
})

--mp40(second best)--
RTS.new_weapon( "Submachinegun_mp40", {
	Name = "Submachinegun_mp40",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"mp40_fire.ogg"},
	Damage = 0.6,
	Delay = 0.4,
	Speed = 7,
})



------------------------------------------------------------------------------Machine guns----------------------------------------------------------------------------



--Machinegun-Browning(second best)--
RTS.new_weapon( "machinegun_browning", {
	Name = "Browning",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"Browning_fire.ogg"},
	Damage = 1.5,
	Delay = 0.3,
	Speed = 7,
})

--Machinegun-mg42(best)--
RTS.new_weapon( "machinegun_mg42", {
	Name = "Mg42",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"mg42_fire.ogg"},
	Damage = 2,
	Delay = 0.2, 
	Speed = 7,
})

--Machinegun-SAW(third best)--
RTS.new_weapon( "machinegun_saw", {
	Name = "SAW",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"SAW_fire.ogg"},
	Damage = 1,
	Delay = 0.5,
	Speed = 7,
})





------------------------------------------------------------------------------Snipers----------------------------------------------------------------------------





--Sniper-270(second best)--
RTS.new_weapon( "Sniper_270", {
	Name = "Sniper_270",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"270_fire.ogg"},
	Damage = 8,
	Delay = 7,
	Speed = 7,
})

--Sniper-50cal(best)--
RTS.new_weapon( "Sniper_50cal", {
	Name = "Sniper_50cal",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"50cal_fire.ogg"},
	Damage = 10,
	Delay = 8,
	Speed = 7,
})

--Sniper-Dragunov(third best)--
RTS.new_weapon( "Sniper_dragunov", {
	Name = "Sniper_Dragunov",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Sounds = {"Dragunov_fire.ogg"},
	Damage = 6,
	Delay = 6,
	Speed = 7,
})

--SHOTGUNS--

	--Shotgun-pump--
	RTS.new_weapon( "Shotgun_12gauge", {
		Name = "12 Gauge Shotgun",
		Race = "Human",
		Bullet = "backgrounds/bullet_real.png",
		Image = "weapons/12gauge.png",
		BulletOrigin = {14,14},
		Sounds = {fire="12gauge_fire.ogg",reload="12gauge_reload.ogg"},
		ClipSize = 1,
		ReloadTime = 2,
		Damage = 5,
		Speed = 3,
		Range = 15,
	})

	--Shotgun-full-auto--
	RTS.new_weapon( "Shotgun_AA12", {
		Name = "AA12 Shotgun",
		Race = "Human",
		Bullet = "backgrounds/bullet.png",
		Sounds = {"AA12_fire.ogg"},
		Damage = 3,
		Delay = 0.3,
		Speed = 4,
		Range = 15,
	})

	--Shotgun-double barrel--
	RTS.new_weapon( "Shotgun_dbbarrel", {
		Name = "Double Barreled Shotgun",
		Race = "Human",
		Image = "weapons/double_barrel.png",
		Bullet = "backgrounds/bullet_real.png",
		Sounds = {fire="dbbarrel_fire.ogg",reload="dbbarrel_reload.ogg"},
		ClipSize = 2,
		ReloadTime = 3,
		Damage = 3,
		Delay = 0.5,
		Speed = 6,
		Range = 15,
	})

--SHOTGUNS--

	--Shotgun-pump--
	RTS.new_weapon( "shotgun", {
		Name = "Shotgun",
		Race = "Human",
		Bullet = "backgrounds/bullet_real.png",
		Image = "weapons/12gauge.png",
		BulletOrigin = {14,14},
		Sounds = {fire="12gauge_fire.ogg",reload="12gauge_reload.ogg"},
		ClipSize = 1,
		Delay = 0.5,
		ReloadTime = 2,
		Damage = 4,
		Speed = 3,
		Range = 15,
	})

	--Shotgun double barrel
	RTS.new_weapon( "shotgun_double", {
		Name = "Double Barrel Shotgun",
		Race = "Human",
		Bullet = "backgrounds/bullet_real.png",
		Image = "weapons/double_barrel.png",
		BulletOrigin = {14,14},
		Sounds = {fire="12gauge_fire.ogg",reload="12gauge_reload.ogg"},
		ClipSize = 2,
		Delay = 0.5,
		ReloadTime = 2,
		Damage = 4,
		Speed = 3,
		Range = 15,
	})

	--shotgun triple barrel
	RTS.new_weapon( "shotgun_triple", {
		Name = "Triple Barrel Shotgun",
		Race = "Human",
		Bullet = "backgrounds/bullet_real.png",
		Image = "weapons/triple_barrel.png",
		BulletOrigin = {14,14},
		Sounds = {fire="12gauge_fire.ogg",reload="12gauge_reload.ogg"},
		ClipSize = 3,
		Delay = 0.5,
		ReloadTime = 2,
		Damage = 4,
		Speed = 3,
		Range = 15,
	})






--PISTOLS--

	--Dual Wield Pistol--
	RTS.new_weapon( "dual_pistols", {
		Name = "Dual Wield Pistols",
		Race = "Human",
		Bullet = "backgrounds/bullet_real.png",
		Image = "weapons/dual_pistols.png",
		BulletOrigin = {14,14,14,-14},
		Sounds = {fire="pistol_fire.ogg"},
		ClipSize = 6,
		Delay = 0.5,
		ReloadTime = 2,
		Damage = 1,
		Speed = 3,
		Range = 30,
	})

--colt--
RTS.new_weapon( "pistol_colt", {
	Name = "pistol_colt",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Damage = 1,
	Sounds = {"pistol_fire.ogg"},
	Delay = 2,
	Speed = 3,
})

--Desert_Eagle--
RTS.new_weapon( "pistol_deserteagle", {
	Name = "pistol_deserteagle",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Damage = 4,
	Sounds = {"desserteagle_fire.ogg"},
	Delay = 3,
	Speed = 3,
})

--Luger--
RTS.new_weapon( "pistol_luger", {
	Name = "pistol_luger",
	Race = "Human",
	Bullet = "backgrounds/bullet.png",
	Damage = 1,
	Sounds = {"Luger_fire.ogg"},
	Delay = 2,
	Speed = 3,
})
