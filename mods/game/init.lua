modpath = RTS.get_modpath

require("weapons.lua")
require("units.lua")
require("buildings.lua")

--Tree--
RTS.new_object( "Tree", {
	Name = "Tree",
	Type = "Flora",
	Image = "tree.png",
	CollisionBox = {"circle", 10},
	ImageOrigin = {50,50},
})

RTS.new_race( "Human", {
	Name = "Human",
	Images = {"head.png","body.png","gun.png"},
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},
})
