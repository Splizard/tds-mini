------------------------------------------------------------------------------Special units----------------------------------------------------------------------------

--Flame thrower unit--
RTS.new_unit( "Flamethrower_unit", {
	Name = "Flamethrower_unit",
	Race = "Human",
	Images = {"good/Flamethrower.png", "bad/Flamethrower.png"},

	Speed = 20,
	RotateSpeed = 4,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Flamethrower",
	Range = 10,
})

--Villager--
RTS.new_unit( "villager", {
	Name = "Villager",
	Race = "Human",
	Type = "Builder",
	Description = "Builds and gathers resources.",
	Cost = 5,
	
	Builds = {"command_post","barracks"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	
	Weapon = "hands",
})

--Commander--
RTS.new_unit( "commander", {
	Name = "Villager",
	Race = "Human",
	Type = "hero",
	Description = "Two words: owns up",
	Cost = 50,

	Speed = 11,
	RotateSpeed = 7,

	Health = 20,
	Weapon = "dual_pistols",
	Commander = true,
})

--Tank--
RTS.new_vehicle( "tank", {
	Name = "Tank",
	Race = "Human",
	Type = "vehicle",
	Images = {body="vehicles/body.png",head="vehicles/head.png"},
	Description = "Two words: owns up",
	Cost = 50,

	Speed = 11,
	RotateSpeed = 7,
	CollisionBox = {"rect", 144,96},
	ImageOrigin = {144/2,96/2},
	SelectionCircle = 144/2,

	Health = 100,
	Weapon = "dual_pistols",
	Commander = true,
})


------------------------------------------------------------------------------Sub machine gunners(smg's)----------------------------------------------------------------------------



--ak47 unit--
RTS.new_unit( "Ak47_unit", {
	Name = "Ak47_unit",
	Race = "Human",
	Images = {"good/ak47.png", "bad/ak47.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Submachinegun_ak47",
	Range = 40,
})

--mp40 unit--
RTS.new_unit( "mp40_unit", {
	Name = "mp40_unit",
	Race = "Human",
	Images = {"good/mp40.png", "bad/mp40.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Submachinegun_mp40",
	Range = 30,
})

--Type100 unit--
RTS.new_unit( "Type100_unit", {
	Name = "Type100_unit",
	Race = "Human",
	Images = {"good/Type100.png", "bad/Type100.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Submachinegun_type100",
	Range = 25,
})





------------------------------------------------------------------------------Machine gunners----------------------------------------------------------------------------



--browning unit--
RTS.new_unit( "Browning_unit", {
	Name = "Browning_unit",
	Race = "Human",
	Images = {"good/Browning.png", "bad/Browning.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "machinegun_browning",
	Range = 30,
})

--mg42 unit--
RTS.new_unit( "Mg42_unit", {
	Name = "Mg42_unit",
	Race = "Human",
	Images = {"good/Mg42.png", "bad/Mg42.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "machinegun_mg42",
	Range = 35,
})

--SAW unit--
RTS.new_unit( "SAW_unit", {
	Name = "SAW_unit",
	Race = "Human",
	Images = {"good/SAW.png", "bad/SAW.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "machinegun_saw",
	Range = 20,
})






-----------------------------------------------------------------------Pistols-----------------------------------------------------------------------------------








--colt--
RTS.new_unit( "Colt_unit", {
	Name = "Colt_unit",
	Race = "Human",
	Images = {"good/colt.png", "bad/colt.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "pistol_colt",
	Range = 30,
})

--Desert Eagle--
RTS.new_unit( "Deserteagle_unit", {
	Name = "Deserteagle_unit",
	Race = "Human",
	Images = {"good/Deserteagle.png", "bad/Deserteagle.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "pistol_deserteagle",
	Range = 50,
})

--Desert Eagle--
RTS.new_unit( "Luger_unit", {
	Name = "Luger_unit",
	Race = "Human",
	Images = {"good/Luger.png", "bad/Luger.png"},

	Speed = 10,
	RotateSpeed = 7,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "pistol_luger",
	Range = 50,
})





------------------------------------------------------------------------Shotgunners------------------------------------------------------------------------------







--Shotgunner light
RTS.new_unit( "shotgunner_light", {
	Name = "Light Shotgunner",
	Race = "Human",
	Type = "Shotgun",
	Description = "Weak close-ranged unit.",
	Cost = 10,

	Speed = 10,
	RotateSpeed = 6,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 8,
	Weapon = "shotgun",
})

--Shotgunner
RTS.new_unit( "shotgunner", {
	Name = "Shotgunner",
	Race = "Human",
	Description = "Close-ranged unit.",
	Cost = 20,

	Speed = 10,
	RotateSpeed = 6,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "shotgun_double",
})

--Shotgunner heavy
RTS.new_unit( "shotgunner_heavy", {
	Name = "Heavy Shotgunner",
	Race = "Human",
	Description = "Powerful close-ranged unit.",
	Cost = 30,

	Speed = 10,
	RotateSpeed = 6,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 12,
	Weapon = "shotgun_triple",
})



------------------------------------------------------------------------------Snipers----------------------------------------------------------------------------




--Sniper-270--
RTS.new_unit( "270_unit", {
	Name = "270_unit",
	Race = "Human",
	Images = {"good/270.png", "bad/270.png"},

	Speed = 4,
	RotateSpeed = 10,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Sniper_270",
	Range = 40,
	MinRange = 10,
})

--Sniper-Dragunov--
RTS.new_unit( "Dragunov_unit", {
	Name = "Dragunov_unit",
	Race = "Human",
	Images = {"good/Dragunov.png", "bad/Dragunov.png"},

	Speed = 4,
	RotateSpeed = 10,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Sniper_dragunov",
	Range = 30,
	MinRange = 10,
})

--Sniper-50cal--
RTS.new_unit( "50cal_unit", {
	Name = "50cal_unit",
	Race = "Human",
	Images = {"good/50cal.png", "bad/50cal.png"},

	Speed = 4,
	RotateSpeed = 10,
	CollisionBox = {"circle", 18},
	ImageOrigin = {24,24},

	Health = 10,
	Weapon = "Sniper_50cal",
	Range = 80,
	MinRange = 10,
})
