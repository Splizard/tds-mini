--Barracks--
RTS.new_building( "barracks", {
	Name = "Barracks",
	Race = "Human",
	Health = 30,
	Cost = 100,
	Image = "buildings/barracks.png",
	CollisionBox = {"rect", 144,96},
	ImageOrigin = {144/2,96/2},
	Trains = {"shotgunner_light","shotgunner","shotgunner_heavy"},
})

--Barracks--
RTS.new_building( "command_post", {
	Name = "Command Post",
	Race = "Human",
	Health = 100,
	Cost = 500,
	Image = "buildings/command_post.png",
	CollisionBox = {"rect", 144,144},
	ImageOrigin = {144/2,144/2},
	Trains = {"villager","commander"},
})
