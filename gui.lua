gui = {}
gui._lists = {}
gui._lables = {}
gui._textBoxes = {}

gui.__textBox = nil

--TextBox Entry
gui.keypressed = function(key, unicode)
	if gui.__textBox then
		local __textBox = gui.__textBox
		--[[if unicode > 31 and unicode < 127 then
			if not (__textBox.limit > 0 and #__textBox.txt >= __textBox.limit) then
				__textBox.txt = __textBox.txt .. string.char(unicode)
			end
		end]]
		if key == "backspace" then
			__textBox.txt = __textBox.txt:sub(1,-2)
		end
		if key == "return" then
			__textBox.callback(tostring(__textBox))
			--__textBox = nil
		end
	end
end

gui.update = function()

end

gui.clear = function()
	gui._lists = {}
	gui._lables = {}
	gui._textBoxes = {}
	gui.__textBox = nil
end

gui.draw = function()
	--Draw Lists
	for i,self in pairs(gui._lists) do
		if self.font then love.graphics.setFont(self.font) end
		love.graphics.setColor(self.colour)
		love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
		local y = 0
		for i,v in ipairs(self.txt) do
			if i >= self.start then
				if not (y+self.fontSize >= self.height)then
					love.graphics.setColor({255,255,255})
					love.graphics.printf(v:sub(1,limit),self.x,self.y+y,self.width,self.allign)
					y = y + self.fontSize
				else
					self.start = self.start + 1 
				end
			end
		end
	end
	--Draw Text Boxes
	for i,self in pairs(gui._textBoxes) do
		self:draw()
	end
	--Draw lables
	for i,self in pairs(gui._lables) do
		self:draw()
	end
end

--Text Box
gui.TextBox = {}
gui.TextBox.__index = gui.TextBox
function gui.newTextBox(x,y,width,height,colour)
	local txtbox = {}
	setmetatable(txtbox,gui.TextBox)
	txtbox.txt = ""
	txtbox.x = x
	txtbox.y = y
	txtbox.width = width
	txtbox.height = height
	txtbox.colour = colour or {200, 200, 200}
	txtbox.limit = 0
	txtbox.callback = function() end
	txtbox.txtColour = {0, 0, 0}
	table.insert(gui._textBoxes, txtbox)
	return txtbox
end
function gui.TextBox:setCallback(callback)
	if type(callback) ~= "function" then return end
	self.callback = callback
end
function gui.TextBox:setText(txt)
	self.txt = tostring(txt) or ""
end
function gui.TextBox:setTextColour(txt)
	self.txtColour = txt 
end
function gui.TextBox:setFont(font)
	self.font = font
end
function gui.TextBox:setLimit(limit)
	self.limit = tonumber(limit) or 0
end
function gui.TextBox:getText()
	return self.txt
end
function gui.TextBox:__tostring()
	return self.txt
end
function gui.TextBox:focus()
	gui.__textBox = self
end
function gui.TextBox:draw()
	--love.graphics.setColorMode("modulate")
	love.graphics.setColor(self.colour[1],self.colour[2],self.colour[3])
	love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
	if self.font then love.graphics.setFont(self.font) end
	love.graphics.setColor(self.txtColour)
	love.graphics.print(self.txt:sub(1,limit).."|",self.x,self.y)
	love.graphics.setColor(255,255,255)
end

--Lable
gui.Lable = {}
gui.Lable.__index = gui.Lable
function gui.newLable(x,y,width,allign,colour)
	local lble = {}
	setmetatable(lble,gui.Lable)
	lble.txt = ""
	lble.x = x
	lble.y = y
	lble.allign = allign or "left"
	lble.width = width
	--lble.height = height
	lble.colour = colour or {200, 200, 200}
	lble.limit = 0
	table.insert(gui._lables, lble)
	return lble
end
function gui.Lable:setText(txt)
	self.txt = tostring(txt) or ""
end
function gui.Lable:setFont(font)
	self.font = font
end
function gui.Lable:setLimit(limit)
	self.limit = tonumber(limit) or 0
end
function gui.Lable:getText()
	return self.txt
end
function gui.Lable:__tostring()
	return self.txt
end
function gui.Lable:draw()
	if self.font then love.graphics.setFont(self.font) end
	love.graphics.setColor(self.colour)
	love.graphics.printf(self.txt:sub(1,limit),self.x,self.y,self.width,self.allign)
end

--List

gui.List = {}
gui.List.__index = gui.List
function gui.newList(x,y,width,height,allign,colour)
	local lst = {}
	setmetatable(lst,gui.List)
	lst.txt = {}
	lst.x = x
	lst.y = y
	lst.allign = allign or "left"
	lst.width = width
	lst.height = height
	lst.colour = colour or {100, 100, 100}
	lst.limit = 0
	lst.fontSize = 24
	lst.start = 1
	table.insert(gui._lists,lst)
	return lst
end
function gui.List:addText(txt)
	table.insert(self.txt, tostring(txt) or "")
end
function gui.List:setFont(font,size)
	self.font = font
	self.fontSize = size
end
function gui.List:reset()
	self.txt = {}
end
--~ function gui.Lable:setLimit(limit)
	--~ self.limit = tonumber(limit) or 0
--~ end
--~ function gui.Lable:getText()
	--~ return self.txt
--~ endd

return gui
