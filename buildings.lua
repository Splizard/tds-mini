buildingsInGame = {} --Buildings

--Collision
function buildingHit(unit,bullet)
	local id = tonumber(string.match(unit, "%d+"))
	local ID = tonumber(string.match(bullet, "%d+"))
	if not bulletsInGame[ID].dead then
		if buildingsInGame[id].team ~= bulletsInGame[ID].team then
			buildingsInGame[id].health = buildingsInGame[id].health - (bulletsInGame[ID].damage * (buildingsInGame[id].resistance[bulletsInGame[ID].type] or 1)) --Remove health
		end
		bulletsInGame[ID].body:setLinearDamping(2) --Stop bullet
		bulletsInGame[ID].dead = true --Destroy bullet
	else
		bulletsInGame[ID].dead = true
	end
end

destroybuilding = function(U,id)
	buildingsInGame[id].body:destroy()
	table.remove(buildingsInGame,id)
	if U.reload == true then sounds[U.weapon][2]:stop() end
	for ID, U in pairs(unitsInGame) do

		--Remove targets to this unit
		if U.target == id and U.targetType == "building" then
			U.target = nil
		end
		if U.target ~= nil and U.target > id and U.targetType == "building" then
			U.target = U.target - 5
		end
		if U.waypoint ~= nil and U.waypoint[U.flag] ~= nil and U.waypoint[U.flag].target == id and U.waypoint[U.flag].targetType == "building" then
			U.waypoint[U.flag].target = nil
			U.flag = U.flag + 1
		end
		if U.waypoint ~= nil then
			for id,W in pairs(U.waypoint) do
				if W.target ~= nil and W.target > id and W.targetType == "building" then
					W.target = W.target - 1
				end
			end
		end
	end
	for ID, B in pairs(bulletsInGame) do

		--Remove targets to this unit
		if B.shooter == id then B.shooter = nil end
		if B.shooter ~= nil and B.shooter > id then B.shooter = B.shooter-1 end
	end
	if alpha == id and alphaType == "building" then alpha = nil end
end

function buildingStep(dt)
	--BBILDING STEP--
	for id,B in pairs(buildingsInGame) do

		if B.ghost then
			B.body:setX(mouseX)
			B.body:setY(mouseY)
		end

		if B.placing then
			B.rotate = math.angle(B.body:getX(),B.body:getY(),x,y)+(math.pi/2)
		end

		if B.body:getAngle() ~= B.rotate then
			B.body:setAngle(B.rotate)
		end

		if B.built == false then
			for i,U in pairs(unitsInGame) do
				if units[U.id].builds then
					if love.physics.getDistance(U.fixture,B.fixture) < 10 then
						B.health = B.health + 0.01
					end
				end
			end
			if B.health >= buildings[B.id].health then
				B.health = buildings[B.id].health
				B.built = true
			end
		end

		--Destroy bBilding if it is dead (and fix lose ends).
		if B.health < 1 or (B.selected and (love.keyboard.isDown("delete")) and B.team == player[control].team)  then
			destroybuilding (B,id)
		end
	end
end

function drawBuildings(lg)
	--Draw buildings.
	for id,B in pairs(buildingsInGame) do
		topLeftX, topLeftY, bottomRightX, bottomRightY = B.shape:computeAABB( 0, 0, B.rotate)
		if B.safeToPlace > 0 or B.body:getX() > mapWidth or B.body:getX() < 0 or B.body:getY() > mapHeight or B.body:getY() < 0 then
			lg.setColor( 255, 0, 0)
			lg.setColorMode("modulate")
		end
		if B.built then
			lg.draw(images[B.id][1],B.body:getX(), B.body:getY(), B.rotate,1,1,B.origin[1],B.origin[2])
		else
			lg.translate( B.body:getX(), B.body:getY() )
			lg.rotate( B.rotate )
			lg.setColor( 50, 50, 50)
			lg.rectangle("line",-buildings[B.id].box[2]/2,-buildings[B.id].box[3]/2, buildings[B.id].box[2],buildings[B.id].box[3],0)
			lg.rotate( -B.rotate )
			lg.translate( -B.body:getX(), -B.body:getY() )
		end
		lg.setColorMode("replace")
		if B.selected then
			lg.translate( B.body:getX(), B.body:getY() )
			lg.rotate( B.rotate )
			bar = true
			--Health bar
			lg.setColor( 0, 255, 0)
			if B.health > 0 then lg.rectangle("fill",-buildings[B.id].box[2]/2, buildings[B.id].box[3]/2+4, buildings[B.id].box[2]/buildings[B.id].health*(B.health), 7) end

			--Selection circle
			if B.team == player[control].team then
				lg.setColor( 0, 0, 255)
			else
				lg.setColor( 255, 0, 0)
			end
			if not B.built then
				lg.setColor( 50, 50, 50)
				lg.rectangle("line",-buildings[B.id].box[2]/2,-buildings[B.id].box[3]/2, buildings[B.id].box[2],buildings[B.id].box[3],0)
			end
			lg.rectangle("line",-buildings[B.id].box[2]/2,-buildings[B.id].box[3]/2, buildings[B.id].box[2],buildings[B.id].box[3],0)
			lg.rotate( -B.rotate )
			lg.translate( -B.body:getX(), -B.body:getY() )
		end
	end
end

function drawBuildingsMinimap(lg)
	--Draw buildings on minimap.
	for id,B in pairs(buildingsInGame) do
		if B.team == player[control].team then
			if B.selected == true then
				lg.setColor( 255, 255, 255)
			else
				lg.setColor( 0, 0, 255)
			end
		else
			lg.setColor( 255, 0, 0)
		end
		lg.rectangle("fill",
			( ( ( B.body:getX() - B.origin[1] ) / mapWidth ) * miniMapWidth ) + miniMapX + 0,
			( ( ( B.body:getY() - B.origin[2] ) / mapHeight ) * miniMapHeight ) + miniMapY + 0,
			( ( B.box[2] / mapWidth ) * miniMapWidth ),
			( ( B.box[3] / mapHeight ) * miniMapHeight )
		)
	end
end

function drawBuildingsHud(lg)
	if barPos > 0 then lg.print(buildingsInGame[alpha].name, 2+0-100+barPos, 4+0) end
	lg.print("Building", 2+0-100+barPos, 16+0)
	lg.print("Health: "..math.round(buildingsInGame[alpha].health), 2+0-100+barPos, 134+0)
	--lg.print("Debug: "..(buildingsInGame[alpha].fixture:getUserData() or "nil"), 5+0-100+barPos, 146+0)
	--lg.print("Debug: "..(alpha), 21+0-100+barPos, 158+0)
	local scaleX = 1
	local scaleY = 1
	if buildingsInGame[alpha].box[2] > 100 then scaleX = 100/buildingsInGame[alpha].box[2] end
	if buildingsInGame[alpha].box[3] > 100 then scaleY = 100/buildingsInGame[alpha].box[3] end
	if scaleX < scaleY then scale = scaleX else scale = scaleY end
	lg.draw(images[buildingsInGame[alpha].id][1],50+0-100+barPos, 80, math.pi,scale,scale,buildingsInGame[alpha].origin[1],buildingsInGame[alpha].origin[2])
end
