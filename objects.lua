objectsInGame = {}

--Check if object is within camera.
local function unitInBounds(x,y,unit)
	if (x+(unit.origin[1]*camera.scaleX)) < camera.x or 
	(x-(unit.origin[1]*camera.scaleX)) > (camera.x+(windowWidth*camera.scaleX)) then
		return false
	end
	if (y+(unit.origin[2]*camera.scaleY)) < camera.y or 
	(y-(unit.origin[2]*camera.scaleY)) > (camera.y+(windowHeight*camera.scaleY)) then
		return false
	end
	return true
end

function drawObjects(lg)
	for id, object in pairs(objectsInGame) do

		local x = object.x or object.body:getX()
		local y = object.y or object.body:getY()
		
		if not (object.invisible) or state == "editor" then 
			if unitInBounds(x,y,object) then
				--Draw body.
				if images[object.id] and images[object.id][1] then
					lg.draw(images[object.id][1],x,y,0,1,1,object.origin[1],object.origin[2])
				end
			end
		end
	end
end
