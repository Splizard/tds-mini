if client then game.log = function() end end

RTS = {}
UNIT = {}
OBJECT = {}
units = {}
images = {}
objects = {}
weapons = {}
sounds = {}
races = {}
buildings = {}
missingImage = love.graphics.newImage("data/images/UnknownUnit.png")

RTS.new_image = function(filepath)
	if love.filesystem.exists(filepath) then
		local image = love.filesystem.newFile(filepath)
		return love.graphics.newImage(image)
	else
		game.log(filepath..": Image not found!", "error")
		return missingImage
	end
end

RTS.new_sound = function(filepath)
	if love.filesystem.exists(filepath) then
		local sound = love.filesystem.newFile(filepath)
		local mode = "static"
		if sound:getSize() > 1048576 then mode = "stream" end
		return love.audio.newSource(sound)
	else
		game.log(filepath..": Sound not found!", "error")
		return nil
	end
end

--Create a new unit--
RTS.new_unit = function(name, attributes,ex)
	if tostring(name) == nil then io.write("[ERROR] "..RTS.get_modname..": name required when creating new units!\n") return end

	--Load unit
	units[name] = {}
	units[name].id = tostring(name)
	units[name].get = attributes
	units[name].name = tostring(attributes.Name or "Undercover unit")
	units[name].description = tostring(attributes.Description or "")
	units[name].race = tostring(attributes.Race or "Unknown")
	units[name].cost = tonumber(attributes.Cost or 20)
	units[name].rev = tonumber(attributes.RotateSpeed or 0.07)*0.5
	units[name].speed = tonumber(attributes.Speed or 10)*10
	units[name].commander = (attributes.Commander and true) or false
	
	units[name].los = tonumber(attributes.LOS or 0)*10
	units[name].heals = attributes.Heals or false

	units[name].health = tonumber(attributes.Health or 10)
	units[name].shield = tonumber(attributes.Shield or 0)
	units[name].armour = tonumber(attributes.Armour or 0)
	units[name].rechargeSpeed = tonumber(attributes.RechargeRate or 0)

	units[name].weapon = tostring(attributes.Weapon or "Unarmed")
	if weapons[units[name].weapon] == nil then 
		game.log("[ERROR] "..RTS.get_modname..": "..attributes.Weapon..": weapon not found!\n") 
		weapons[units[name].weapon] = "Unarmed" 
		return 
	end
	units[name].resistance = attributes.Resistance or {}
	units[name].builds = attributes.Builds
	
	if attributes.Images then
		images[name] = {}
		if attributes.Images["head"] then
			images[name].head = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images["head"])
		end
		if attributes.Images["body"] then
			images[name].body = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images["body"])
		end
		if attributes.Images["gun"] then
			images[name].gun = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images["gun"])
		end
	end
	
end

--Create a new vehicle--
RTS.new_vehicle = function(name, attributes,ex)
	if tostring(name) == nil then game.log(RTS.get_modname..": name required when creating new units!", "error") return end

	--Load unit
	units[name] = {}
	units[name].id = tostring(name)
	units[name].get = attributes
	units[name].name = tostring(attributes.Name or "Undercover vehicle")
	units[name].description = tostring(attributes.Description or "")
	units[name].race = tostring(attributes.Race or "Unknown")
	units[name].cost = tonumber(attributes.Cost or 20)
	units[name].rev = tonumber(attributes.RotateSpeed or 0.07)*0.01
	units[name].speed = tonumber(attributes.Speed or 10)*10
	units[name].commander = (attributes.Commander and true) or false
	
	units[name].los = tonumber(attributes.LOS or 0)*10
	units[name].heals = attributes.Heals or false

	units[name].health = tonumber(attributes.Health or 10)
	units[name].fuel = tonumber(attributes.Fuel or 0)
	units[name].shield = tonumber(attributes.Shield or 0)
	units[name].armour = tonumber(attributes.Armour or 0)
	units[name].rechargeSpeed = tonumber(attributes.RechargeRate or 0)
	
	units[name].box = attributes.CollisionBox
	units[name].origin = attributes.ImageOrigin
	units[name].selectionCircle = attributes.SelectionCircle

	units[name].weapon = tostring(attributes.Weapon or "Unarmed")
	--TODO: Multiweapons
	--~	units[name].weapons = tonumber
--~	units[name].weapon = {}
--~	for i=1, units[name].weapons do
--~		units[name].weapon[i] = tostring(attributes.Weapon[i] or "Unarmed")
--~		if weapons[units[name].weapon][i] == nil then 
--~			io.write("[ERROR] "..RTS.get_modname..": "..attributes.Weapon[i]..": weapon not found!\n") 
--~			weapons[units[name].weapon][i] = "Unarmed" 
--~			return 
--~		end
	if weapons[units[name].weapon] == nil then 
		game.log(RTS.get_modname..": "..attributes.Weapon..": weapon not found!","error") 
		weapons[units[name].weapon] = "Unarmed" 
		return 
	end
	units[name].resistance = attributes.Resistance or {}
	units[name].builds = attributes.Builds
	
	if attributes.Images then
		images[name] = {}
		if attributes.Images["head"] then
			images[name].head = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images["head"])
		end
		if attributes.Images["body"] then
			images[name].body = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images["body"])
		end
		if attributes.Images["gun"] then
			images[name].gun = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images["gun"])
		end
	end
end

RTS.new_race = function(name, attributes)
	races[name] = {}
	races[name].box = {}
	races[name].box[1] = tostring(attributes.CollisionBox[1] or "circle")
	races[name].box[2] = tonumber(attributes.CollisionBox[2] or 18)
	if races[name].box[1] == "circle" then
		races[name].box[3] = tonumber(attributes.CollisionBox[2])*2
	else
		races[name].box[3] = tonumber(attributes.CollisionBox[3])
	end
	races[name].selectionCircle = tonumber(attributes.SelectionCircle or 24)
	races[name].origin = {}
	races[name].origin[1] = tonumber(attributes.ImageOrigin[1] or 0)
	if races[name].box[1] == "circle" then
		races[name].origin[2] = races[name].origin[1]
	else
		races[name].origin[2] = tonumber(attributes.ImageOrigin[2] or 0)
	end
	images[name] = {}
	images[name].head = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images[1])
	images[name].body = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images[2])
	images[name].gun = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Images[3])
end

RTS.new_weapon = function(name, attributes)
	if tostring(name) == nil then game.log(RTS.get_modname..": name required when creating new weapons!", "error") return end
	weapons[name] = {}
	weapons[name].id = name
	weapons[name].get = attributes
	weapons[name].name = tostring(attributes.Name) or "Unidentified weapon"
	weapons[name].damage = tonumber(attributes.Damage or 1)
	weapons[name].antiArmour = tonumber(attributes.AntiArmour or attributes.Damage or 1)
	weapons[name].delay = tonumber(attributes.Delay or 0)
	weapons[name].origin = {}
	if attributes.BulletOrigin ~= nil then
		weapons[name].origin[1] = tonumber(attributes.BulletOrigin[1] or 0)
		weapons[name].origin[2] = tonumber(attributes.BulletOrigin[2] or 0)
		weapons[name].origin[3] = tonumber(attributes.BulletOrigin[3] or nil)
		weapons[name].origin[4] = tonumber(attributes.BulletOrigin[4] or nil)
	else weapons[name].origin = {0,0} end
	weapons[name].speed = tonumber(attributes.Speed or 3)
	weapons[name].clipSize = tonumber(attributes.ClipSize or 0)
	weapons[name].maxAmmo = tonumber(attributes.MaxAmmo or 0)
	weapons[name].burst = tonumber(attributes.Burst or 0)
	weapons[name].burstDelay = tonumber(attributes.BurstDelay or 0)
	weapons[name].reloadTime = tonumber(attributes.ReloadTime or 0)
	weapons[name].type = tostring(attributes.Type or "Melee")
	weapons[name].mode = tostring(attributes.Mode or "Ranged")
	
	weapons[name].exSize = tonumber(attributes.ExplosionSize or 0)*10
	
	weapons[name].range = tonumber(attributes.Range or 30)*10
	weapons[name].minRange = tonumber(attributes.MinRange or 0)*10
	weapons[name].spread = tonumber(attributes.Spread or 0)/10
	weapons[name].maxRange = tonumber(attributes.MaxRange or 0)	

	--Load bullets image
	images[name] = {}
	if attributes.Bullet ~= nil then
		images[name][1] = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Bullet)
	end

	sounds[name] = {}
	if attributes.Sounds ~= nil and attributes.Sounds[1] ~= nil then
		sounds[name][1] = RTS.new_sound("mods/"..RTS.get_modname.."/sounds/"..attributes.Sounds[1], "static")
	elseif attributes.Sounds ~= nil then
		if attributes.Sounds.fire ~= nil then
			sounds[name][1] = RTS.new_sound("mods/"..RTS.get_modname.."/sounds/"..attributes.Sounds.fire, "static")
		end
		if attributes.Sounds.reload ~= nil then
			sounds[name][2] = RTS.new_sound("mods/"..RTS.get_modname.."/sounds/"..attributes.Sounds.reload, "static")
		end
	end
	if attributes.Image ~= nil then
		images[name][2] = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Image)
	end
end

RTS.new_object = function(name, attributes)
	if tostring(name) == nil then game.log(RTS.get_modname..": name required when creating new objects!", "error") return end
	objects[name] = {}
	objects[name].id = name
	objects[name].get = attributes
	objects[name].type = tostring(attributes.Type or "object")
	objects[name].name = tostring(attributes.Name or "Unidentified "..objects[name].type)
	objects[name].sensor = attributes.Sensor or false
	objects[name].box = {}
	objects[name].box[1] = tostring(attributes.CollisionBox[1] or "circle")
	objects[name].box[2] = tonumber(attributes.CollisionBox[2] or 18)
	if objects[name].box[1] == "circle" then
		objects[name].box[3] = tonumber(attributes.CollisionBox[2])*2
	else
		objects[name].box[3] = tonumber(attributes.CollisionBox[2])
	end
	objects[name].invisible = attributes.Invisible or false

	objects[name].origin = {}
	objects[name].origin[1] = tonumber(attributes.ImageOrigin[1] or 0)
	objects[name].origin[2] = tonumber(attributes.ImageOrigin[2] or 0)

	--Load objects image
	images[name] = {}
	images[name][1] = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Image)
end

RTS.new_building = function(name, attributes)
	if tostring(name) == nil then game.log(RTS.get_modname..": name required when creating new buildings!", "error") return end
	buildings[name] = {}
	buildings[name].id = name
	buildings[name].get = attributes
	buildings[name].health = tonumber(attributes.Health or 10)
	buildings[name].race = tostring(attributes.Race or "Unknown")
	buildings[name].cost = tonumber(attributes.Cost or 0)
	buildings[name].description = tostring(attributes.Description or "")
	buildings[name].name = tostring(attributes.Name or "Unidentified "..buildings[name].type)
	buildings[name].resistance = attributes.Resistance or {}
	buildings[name].training = attributes.Trains or {}
	buildings[name].box = {}
	buildings[name].box[1] = tostring(attributes.CollisionBox[1] or "rect")
	buildings[name].box[2] = tonumber(attributes.CollisionBox[2] or 144)
	if buildings[name].box[1] == "circle" then
		buildings[name].box[3] = tonumber(attributes.CollisionBox[2])*2
	else
		buildings[name].box[3] = tonumber(attributes.CollisionBox[3] or 96)
	end

	buildings[name].origin = {}
	buildings[name].origin[1] = tonumber(attributes.ImageOrigin[1] or 0)
	buildings[name].origin[2] = tonumber(attributes.ImageOrigin[2] or 0)

	--Load buildings image
	images[name] = {}
	images[name][1] = RTS.new_image("mods/"..RTS.get_modname.."/images/"..attributes.Image)
end

--Find a spawn point.
--Returns x,y of the spawn.
RTS.get_spawn = function()
	local spawnpoints = {}
	local free_spawnpoints = {}
	for id,O in pairs(objectsInGame) do
		if objects[O.id].type == "spawning" then
			if O.free then
				--This spawnpoint is not blocked by anything.
				table.insert(free_spawnpoints, id)
			end
			table.insert(spawnpoints, id)
		end
	end
	if #free_spawnpoints > 0 then
		local rand = math.random(#free_spawnpoints)
		return objectsInGame[free_spawnpoints[rand]].body:getX(),objectsInGame[free_spawnpoints[rand]].body:getY()
	elseif #spawnpoints > 0 then
		local rand = math.random(#spawnpoints)
		return objectsInGame[spawnpoints[rand]].body:getX(),objectsInGame[spawnpoints[rand]].body:getY()
	else
		return mapWidth/2, mapHeight/2
	end
end

--Respawns unit at a spawnpoint.
RTS.respawn_unit = function(unit)
	local x, y = RTS.get_spawn()
	unit.body:setPosition(x,y)
	unit.target = nil
	unit.health = units[unit.id].health
	unit.armour = units[unit.id].armour
	unit.shield = units[unit.id].shield
	unit.ammo = unit.maxAmmo
	unit.dead = false
	unit.respawning = false
	unit.body:setActive(true)
end

RTS.spawn_bot = function(...)
	RTS.spawn_unit(...)
	id = #unitsInGame
	unitsInGame[id].ai = true
end

RTS.spawn_unit = function(unit,team,x,y,health,rotation,list, gameid)
	--FIXME: create better spawn checker
	--~ local i=1
	--~ local ox = x
	--~ local oy = y
	--~ repeat
		--~ for id,U in pairs(unitsInGame) do
			--~ if CheckCollision(U.body:getX(),U.body:getY(),U.box[2]*2,U.box[2]*2,x,y,races[units[unit].race].box[2]*2,races[units[unit].race].box[2]*2) then
				--~ x=x+U.box[2]*2
				--~ y=y+U.box[2]*2
				--~ spawn = false
			--~ else
				--~ spawn = true
			--~ end
		--~ end
		--~ i = i+1
	--~ until spawn or i == 50
	local list = list or {}
	local U = {
		--Basic
		id = unit,
		class = "unit",
		name = list.name or units[unit].name,
		race = units[unit].race,
		selected = list.selected or false,
		team = team,
		timer = list.timer or 0,
		commander = list.commander or units[unit].commander,
		los = list.los or units[unit].los,
		command = list.command or false,

		--Colision
		body = love.physics.newBody(world, x, y,"dynamic"),
		shape = love.physics.newCircleShape(races[units[unit].race].box[2]),
		box = races[units[unit].race].box,
		origin = races[units[unit].race].origin,
		selectionCircle = races[units[unit].race].selectionCircle,

		--Rotation
		rev = list.rev or units[unit].rev,
		rotate = list.rotate or rotation or 0,
		rotating = list.rotating or false,

		--Combat
		burstTimer = list.burstTimer or 0,
		burst = 0,
		health = health or units[unit].health,
		armour = armour or units[unit].armour,
		shield = list.shield or units[unit].shield,
		shieldTimer = list.shieldTimer or 0,
		target = list.target or nil,
		targetType = list.targetType or nil,
		range = list.range or units[unit].range,
		minRange = list.minRange or units[unit].minrange,
		weapon = list.weapon or units[unit].weapon,
		clipSize = list.clipSize or weapons[units[unit].weapon].clipSize,
		clip = list.clip or weapons[units[unit].weapon].clipSize,
		maxAmmo = list.maxAmmo or weapons[units[unit].weapon].maxAmmo,
		ammo = list.maxAmmo or weapons[units[unit].weapon].maxAmmo,
		reload = list.reload or false,
		reloadTime = list.reloadTime or weapons[units[unit].weapon].reloadTime,
		resistance = list.resistance or units[unit].resistance,
		reloadSound = list.reloadSound or false,

		--Movement
		width = list.width or 48,
		dragging = false,
		moving = list.moving or false,
		speed = list.speed or units[unit].speed or 10*10,
		flag = list.flag or 1,
		waypoint = list.waypoints or nil,
		
		--Multiplayer
		vx = 0,
		vy = 0,
		realX = nil,
		reayY = nil,
		buffer = {[1]={}, [2]={}, [3]={}, [4]={}},
		keys = {},
		shooting = false,
		ai = false
	}
	U.fixture = love.physics.newFixture(U.body, U.shape, 5)
	U.body:setMass(30)
	U.body:setLinearDamping(2)
	if gameid then
		unitsInGame[gameid] = U
		unitsInGame[gameid].fixture:setUserData( "unit "..gameid )
	else
		table.insert(unitsInGame,U)
		id = #unitsInGame
		unitsInGame[id].fixture:setUserData( "unit "..id )
	end
end

RTS.spawn_vehicle = function(unit,team,x,y,health,rotation,list)
	local list = list or {}
	table.insert(unitsInGame,{
		--Basic
		id = unit,
		class = "vehicle",
		name = list.name or units[unit].name,
		race = units[unit].race,
		selected = list.selected or false,
		team = team,
		timer = list.timer or 0,
		commander = list.commander or units[unit].commander,
		los = list.los or units[unit].los,
		command = list.command or false,

		--Colision
		body = love.physics.newBody(world, x, y,"dynamic"),
		shape = love.physics.newRectangleShape(units[unit].box[2],units[unit].box[3]),
		box = units[unit].box,
		origin = units[unit].origin,
		selectionCircle = units[unit].selectionCircle,

		--Rotation
		rev = list.rev or units[unit].rev,
		rotate = list.rotate or rotation or 0,
		rotating = list.rotating or false,

		--Combat
		burstTimer = list.burstTimer or 0,
		burst = 0,
		health = health or units[unit].health,
		armour = armour or units[unit].armour,
		shield = list.shield or units[unit].shield,
		shieldTimer = list.shieldTimer or 0,
		target = list.target or nil,
		targetType = list.targetType or nil,
		range = list.range or units[unit].range,
		minRange = list.minRange or units[unit].minrange,
		weapon = list.weapon or units[unit].weapon,
		clipSize = list.clipSize or weapons[units[unit].weapon].clipSize,
		clip = list.clip or weapons[units[unit].weapon].clipSize,
		maxAmmo = list.maxAmmo or weapons[units[unit].weapon].maxAmmo,
		ammo = list.maxAmmo or weapons[units[unit].weapon].maxAmmo,
		reload = list.reload or false,
		reloadTime = list.reloadTime or weapons[units[unit].weapon].reloadTime,
		resistance = list.resistance or units[unit].resistance,
		reloadSound = list.reloadSound or false,

		--Movement
		width = list.width or 48,
		dragging = false,
		moving = list.moving or false,
		speed = list.speed or units[unit].speed or 10*10,
		flag = list.flag or 1,
		waypoint = list.waypoints or nil,
		
		--Server
		keys = {},
		shooting = false
		
	})
	id = table.getn(unitsInGame)
	unitsInGame[id].fixture = love.physics.newFixture(unitsInGame[id].body, unitsInGame[id].shape, 5)
	unitsInGame[id].body:setMass(30)
	unitsInGame[id].body:setLinearDamping(2)
	unitsInGame[id].fixture:setUserData( "unit "..id )
end

--Spawn bullet.
function RTS.spawn_bullet(weapon,x,y,atX,atY,team,shooter)
	local weapon = weapons[weapon]
	local angle = math.angle(x,y,atX,atY)
	table.insert(bulletsInGame,{
		id = weapon.id,
		name = weapons[weapon.id].name,
		range = weapons[weapon.id].maxRange,
		damage = weapons[weapon.id].damage,
		speed = weapons[weapon.id].speed,
		antiArmour = weapons[weapon.id].antiArmour,
		body = love.physics.newBody(world, x, y,"dynamic"),
		exSize = weapons[weapon.id].exSize,
		shape = love.physics.newCircleShape(5),
		rotate = angle,
		die = false,
		type = weapons[weapon.id].type,
		shooter = shooter,
		fromX = x,
		fromY = y,
		atX = atX,
		atY = atY,
		vecX = nil,
		vecY = nil,
		timer = 0,
		spread = weapon.spread,
		spreadDir = math.random(2),
		team = team,
	})
	id = table.getn(bulletsInGame)
	bulletsInGame[id].fixture = love.physics.newFixture(bulletsInGame[id].body, bulletsInGame[id].shape, 5)
	bulletsInGame[id].body:setMass(0)
	bulletsInGame[id].body:setBullet(true)
	bulletsInGame[id].fixture:setUserData( "bullet "..id )
	bulletsInGame[id].fixture:setSensor( true )
end

RTS.spawn_object = function(object,x,y,health,rotation)
	local O = {
		--Basic
		id = object,
		name = objects[object].name,

		--Colision
		body = love.physics.newBody(world, x, y),
		shape = love.physics.newCircleShape(objects[object].box[2]),
		box = objects[object].box,
		origin = objects[object].origin,
		invisible = objects[object].invisible,
		free = true,
	}
	table.insert(objectsInGame,O)
	--Set friction and damping for units.
	id = table.getn(objectsInGame)
	objectsInGame[id].fixture = love.physics.newFixture(objectsInGame[table.getn(objectsInGame)].body, objectsInGame[table.getn(objectsInGame)].shape, 5)
	if objects[object].sensor then
		objectsInGame[id].fixture:setSensor( true )
		objectsInGame[id].fixture:setUserData("sensor "..id)
	end
end
RTS.spawn_building = function(building,team,x,y,health,rotation,list)
	list = list or {}
	table.insert(buildingsInGame,{
		--Basic
		id = building,
		class = "building",
		race = buildings[building].race,
		name = buildings[building].name,
		selected = false,
		built = list.built or true,
		placing = list.placing or false,
		ghost = list.ghost or false,
		safeToPlace = 0,
		team = team,
		resistance = buildings[building].resistance,

		--Colision
		health = health or buildings[building].health,
		body = love.physics.newBody(world, x, y),
		shape = love.physics.newRectangleShape(buildings[building].box[2],buildings[building].box[3]),
		box = buildings[building].box,
		origin = buildings[building].origin,
		
		--Rotation--
		rotate = rotation or 0,
	})
	--Set friction and damping for units.
	local id = table.getn(buildingsInGame)
	buildingsInGame[id].fixture = love.physics.newFixture(buildingsInGame[id].body, buildingsInGame[id].shape, 5)
	if buildingsInGame[id].ghost == true then buildingsInGame[id].fixture:setSensor(true)  buildingsInGame[id].body:setType( "dynamic" ) end
	buildingsInGame[id].body:setAngle(buildingsInGame[id].rotate)
	buildingsInGame[id].fixture:setUserData( "building "..id )
end

--Unarmed
RTS.new_weapon( "Unarmed", {
	Name = "Unarmed",
	Type = "Unarmed",
	ClipSize = 0,
	Range = 0,
})

--Load all units from race--
Using = function(race)

end

AppDataPath = love.filesystem.getAppdataDirectory( )

local printbak = print
local requirebak = require

print = function(...)
	io.write("  [MOD] ")
	io.write(tostring(...))
	io.write("\n")
end
require = function(file)
	ok, chunk = pcall(love.filesystem.load,"mods/"..RTS.get_modname.."/"..file)
	if not ok then
			game.log('The following error happened: \n\t' .. tostring(chunk).."\n", "error")
		else
			ok, result = pcall(chunk) -- execute the chunk safely
			if not ok then -- will be false if there is an error
				game.log('The following error happened: \n\t' .. tostring(result).."\n", "error")
			end
		end
end

--Load from appdata directory.
mods = love.filesystem.getDirectoryItems("mods/")

for i,v in ipairs(mods) do
	game.log('Loading '..v, "info")
	RTS.get_modname = v 
	RTS.get_modpath = AppDataPath.."/mods/"..v.."/"
	if love.filesystem.isDirectory("mods/"..v) then
		local ok, chunk, result
		ok, chunk = pcall( love.filesystem.load, "mods/"..v.."/init.lua") -- load the chunk safely
		if not ok then
			game.log('The following error happened: \n\t' .. tostring(chunk).."\n", "error")
		else
			ok, result = pcall(chunk) -- execute the chunk safely
			if not ok then -- will be false if there is an error
				game.log('The following error happened: \n\t' .. tostring(result).."\n", "error")
			end
		end
	end
end

print = printbak
require = requirebak
