--Find angle between two points
function math.angle(x1,y1, x2,y2) return math.atan2(y2-y1,x2-x1) end

--Calculate the distance between two points
function math.dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end

--Finds shortest direction to turn
function rotateClosest(a1,a2)
	local dir = math.deg(a1) - math.deg(a2)
	if dir > 0 and math.abs(dir) <= 180 then return("CCW")
	elseif dir > 0 and math.abs(dir) > 180 then return("CW")
	elseif dir < 0 and math.abs(dir) <= 180 then return("CW")
	elseif dir < 0 and math.abs(dir) > 180 then return("CCW") end
end

--Calculate vector to point
function movePlayer(bodyX,bodyY, x, y)
	local function length(v)
		return math.sqrt(v.x*v.x + v.y*v.y)
	end
	local v = {}
	v.x = x-bodyX
	v.y = y-bodyY -- minus gun from here.
	-- normalize the vector.
	local len = length(v)
	v.x = v.x / len
	v.y = v.y / len
	return v
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function split(str, delim)
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then return { str } end

    local result,pat,lastpos = {},"(.-)" .. delim .. "()",nil
    for part, pos in string.gfind(str, pat) do table.insert(result, part); lastPos = pos; end
    table.insert(result, string.sub(str, lastPos))
    return result
end

		
--Fonts are loaded on demand when fonts(font, size) is called.
fonts = {}
fonts.__call = function(self, font, size)
	if self[font..tostring(size)] then
		return self[font..tostring(size)]
	else
		local filename
		if font == "regular" then
			filename = "Ubuntu-R"
		elseif font == "mono" then
			filename = "UbuntuMono-B"
		else
			filename = font
		end
		self[font..tostring(size)]=love.graphics.setNewFont("data/fonts/"..filename..".ttf",size)
		return self[font..tostring(size)]
	end
end
setmetatable(fonts, fonts)

game = {}
	--Logs errors to file and stout.
function game.log(txt, mode)
	if mode == nil then mode = "" end
	if mode == "error" then mode = "[ERROR]" end
	if mode == "info"  then mode = "[INFO]"  end
	txt = tostring(txt) or "nil"
	file = love.filesystem.newFile("log.txt")
	file:open('a')
	if mode == "none" then
		file:write(txt)
	else	
		file:write("["..os.date("%X").."] "..mode.." "..txt.."\n")
		file:close()
		io.write(mode.." "..txt.."\n")
	end
end

--Loads config and returns config values inside table.
function game.loadConfig()
	local config = {}
	local err
	file = love.filesystem.newFile("game.cfg")
	if love.filesystem.exists("game.cfg") then
		file:open('r')
		for line in file:lines() do
			i, v = line:match("^(%S*) = (%S*)")
			if not i or not v then
				err = "Config file could not be parsed"
				break
			end
			if v == "true" then v = true end
			if v == "false" then v = false end
			config[i] = v
		end
		return config, err
	else
		return {}, "Config file does not exist yet!"
	end
end

--Saves contents of client.config to file.
function game.saveConfig(config)
	file = love.filesystem.newFile("game.cfg")
	file:open('w')
	for i,v in pairs(config) do
		local t = type(v)
		if t == "string" or t == "number" or t == "boolean" then
			file:write(i.." = "..tostring(v).."\n")
		else
			print(type(v))
		end
	end
end

---Fix love bug
function love.physics.getDistance(f1,f2)
	return math.dist(f1:getBody():getX(),f1:getBody():getY(),f2:getBody():getX(),f2:getBody():getY())
end

--Timers
game.timers_to_add = {}
game.timers = {}

game.time = function(dtime)
	for _, timer in ipairs(game.timers_to_add) do
		table.insert(game.timers, timer)
	end
	game.timers_to_add = {}
	for index, timer in ipairs(game.timers) do
		timer.time = timer.time - dtime
		if timer.time <= 0 then
			timer.func(timer.param)
			table.remove(game.timers,index)
		end
	end
end

function game.after(time, func, param)
	table.insert(game.timers_to_add, {time=time, func=func, param=param})
end
