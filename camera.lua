--Camera init
camera = {}
camera.x = 0
camera.y = 0
camera.scaleX = 1
camera.scaleY = 1
camera.rotation = 0

camerax = 0
cameray = 0
camerat = love.timer.getTime( )

function camera:set() --Init camera function.
  love.graphics.push()
  love.graphics.rotate(-self.rotation)
  love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
  love.graphics.translate(-self.x, -self.y)
end

function camera:unset() --Deinit camera function.
  love.graphics.pop()
end

function camera:move(dx, dy) --Move camera.
  self.x = self.x + (dx or 0)
  self.y = self.y + (dy or 0)
end

function camera:rotate(dr) --Rotate camera.
  self.rotation = self.rotation + dr
end

function camera:scale(sx, sy) --Scale camera.
  sx = sx or 1
  self.scaleX = self.scaleX * sx
  self.scaleY = self.scaleY * (sy or sx)
end

function camera:setPosition(x, y) --Set camera position.
  self.x = x or self.x
  self.y = y or self.y
end

function camera:setScale(sx, sy) --Set camera scale.
  self.scaleX = sx or self.scaleX
  self.scaleY = sy or self.scaleY
end

function camera:mousePosition() --Get mouse relative coordinates.
	return
	love.mouse.getX() * self.scaleX + self. x,
	love.mouse.getY() * self.scaleY + self.y
end
