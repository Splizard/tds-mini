bulletsInGame = {}

function bulletStep(dt)
	--BULLET STEP--
	for id,B in pairs(bulletsInGame) do
		B.timer = B.timer + dt --Update selftimer.

		if B.dead == true then
			--area of affect weapons
			if B.exSize > 0 then
				B.dead = false
				for ID,E in pairs(unitsInGame) do
					if love.physics.getDistance( B.fixture, unitsInGame[ID].fixture ) <= B.exSize then
						unitHit("unit "..ID,"bullet "..id)
					end
				end
				for ID,E in pairs(bulletsInGame) do
					if love.physics.getDistance( B.fixture, unitsInGame[ID].fixture ) <= B.exSize then
						unitHit("unit "..ID,"bullet "..id)
					end
				end
			end
			B.body:destroy()
			table.remove(bulletsInGame,id)
		else

			--Initalise bullet
			if B.vecX == nil then
				local vec = movePlayer(B.body:getX(),B.body:getY(),B.atX,B.atY)
				B.vecX = vec.x
				B.vecY = vec.y
			end

			--Move bullet
			B.body:setLinearVelocity(B.vecX*50*B.speed,B.vecY*50*B.speed)
			
			if B.maxRange then
				if math.dist(B.body:getX(),B.body:getY(),B.fromX,B.fromY) > B.maxRange then
					B.speed = B.speed - 0.05
				end
				
				if B.speed <= 0 then
					B.dead = true
				end
			end
		end
	end
	
	--Bullet cleanup--
	for id,B in pairs(bulletsInGame) do
		if B.fixture:getUserData() ~= "bullet "..id then
			B.fixture:setUserData("bullet "..id)
		end
	end
end

function drawBullets(lg)
	--Draw bullets.
	for id,B in pairs(bulletsInGame) do
		lg.draw(images[B.id][1],B.body:getX(), B.body:getY(), B.rotate,1,1,5,5)
	end
end
