require "love"
require "love.physics"
require "love.filesystem"
require "love.timer"
love.graphics = {}
love.graphics.newImage = function() end
love.audio = {}
love.audio.newSource = function() end
love.keyboard = {}
love.keyboard.isDown = function() end
require "util"
require "api"
require "units"
require "objects"
require "collision"
require "networking"
require "bullets"
require "map"
require "camera"

dofile("map")

buildingsInGame = {}

local socket = require 'socket'
clients = {}

local udp = socket.udp()
udp:setsockname('*',"31000")
udp:settimeout(0)
ip, port = udp:getsockname()

--Player stuff
	player = {}
	player[0] = {
		team = 0,
		colour = {0, 255, 0},
		keys = {},
		cache = {}
	}
--	player[2] = {
--		team = 2,
--		colour = {255, 0, 0},
--	}
	control = 0 --The player that you are controlling.
	money = 200
	
--Teams
team = {}
team[1] = {
	hostile = {[2]=true},
	friendly = {},
}
team[2] = {
	hostile = {[1]=true},
	friendly = {},
}
teamIsHostile = function(one,two)
	if (one == 0) or (two == 0) then return true end
	if team[one].hostile[two] then
		return true
	else
		return false
	end
end
teamIsFriendly = function(one,two)
	if team[one].friendly[two] then
		return true
	else
		return false
	end
end
	

--Network variables.
local t = 0

local start = os.clock()


local timer = 0
local longtimer = 0
longertimer = 0
local othertimer = 0

local text = ""

--Ticks per minute.
local tickrate = 30
local tick = 1/tickrate
local ticks = 0
local current_tick = 0

--Updates per minute.
updaterate = 20
local update = tickrate/updaterate
local updates = 0
local current_update = 0

dt = tick

local messages_count = 0

game.log("Starting server.", "info")
while true do
	start = os.clock()
	ticks = ticks + 1
	current_tick = current_tick + 1
	updates = updates + 1
	current_update = current_update + 1
	
	timer = timer + dt
	longtimer = longtimer + dt
	longertimer = longertimer + dt
	othertimer = othertimer + dt
	
		--print(dt)

		--Parse incomming data.
		while true do
			data, msg_or_ip, port_or_nil = udp:receivefrom()
			
			if data then
				local id = msg_or_ip..":"..port_or_nil
			
				--Split data into lines.
				for i,data in ipairs(split(data,"\n")) do
				
					--If the client does not exist then create it.
					if not clients[id] then
						clients[msg_or_ip..":"..port_or_nil] = {unit=0, team=0, address=msg_or_ip, port = port_or_nil,messages = {}, send = {}}
					end
					
					local unit = unitsInGame[clients[id].unit]
					data = string.gsub(data, "\n", "")
					--print(data)
					if data then
						cmd, parms = networking.parse(data)
						--Join.
						if cmd == "join" then
							local x,y = RTS.get_spawn()
							RTS.spawn_unit('shotgunner_heavy',clients[id].team,x,y,100,3.1415926535898,{})
							clients[id].send.map = true
							clients[id].unit = #unitsInGame
						--Keypresses.
						elseif cmd == "w" or cmd == "s" or cmd == "a" or cmd == "d" then
							unit.keys[cmd] = true
						elseif cmd == "!"  then
							--print(parms[1])
							unit.keys[parms[1]] = false
							--print(player[i].keys["_"])
						elseif cmd == "_" then
							 unit.keys["_"] = true
							 unit.shooting = true
						--Acknowledgement.
						elseif cmd == "ack" then
							if parms[1] == "map" then
								clients[id].send.map = false
							end
						else
							--print("?command server: "..cmd)
						end
					end
				end
			else
				break
			end
		
		end
	
		local status, err = pcall(unitStep,dt)
		if not status then game.log(err,"error") end
	
		local status, err = pcall(bulletStep,dt)
		if not status then game.log(err,"error") end
		
		game.time(dt)
	
		if ticks > update then
			messages_count = messages_count + 1
			local t = {}
			local tt = #t
			tt = tt + 1
			t[tt] = messages_count
			tt = tt + 1
			--Update for all the units in the game.
			for id,U in pairs(unitsInGame) do
				if not unitsInGame[id].dead then
					t[tt] = "u "..id.." "..U.id.." "..round(U.body:getX(),2).." "..round(U.body:getY(),2).." "..round(U.rotate,2).." "..U.team.. " "..U.health
					tt = tt + 1
					if U.shooting then
						local x, y = U.body:getWorldPoint( 50, 0 )
						t[tt] = "sh "..id
						tt = tt + 1
						U.shooting = false
					end
				else
					t[tt] = "u "..id.." d"
					tt = tt + 1
				end
			end
			local text = table.concat(t, "\n")
			for id,client in pairs(clients) do
				local i = client.unit
				udp:sendto(text.."\nid "..i,client.address,client.port)
				
				--Send map to client.
				if client.send.map then
					local t = {}
					local tt = #t
					tt = tt + 1
					t[tt] = "0.0\nmap"
					tt = tt + 1
					for id,O in pairs(objectsInGame) do
						t[tt] = "o "..O.id.." "..O.body:getX().." "..O.body:getY()
						tt = tt + 1
					end
					local text = table.concat(t, "\n")
					udp:sendto(text,client.address,client.port)
				end
			end
			ticks = 0
		end
		
		if current_tick >= tickrate then
			current_tick = 0
		end
		
		if current_update >= updaterate then
			current_update = 0
		end
		
--		if longtimer > 0.5 then
--			longtimer = 0shotgunner
--		end
		if longertimer > 1 then
			longertimer = 0
			updates = 0
			
			for id,U in pairs(unitsInGame) do
				if not U.dead then
					U.buffer.x = U.body:getX()
					U.buffer.y = U.body:getY()
					U.buffer.r = U.rotate
				end
			end	
		end
	
		world:update(dt)
	

--		if text ~= "" and text ~= " " and othertimer > 0.3 then
--			for id,client in pairs(clients) do
--				local text = text
--				if client.messages then
--					for i,v in ipairs(client.messages) do
--						text = v..text
--					end
--				end
--				udp:sendto(text,id,client.port)
--			end
--			text = ""
--			othertimer = 0
--		end
	
	benchmark = os.clock() - start
	if benchmark > tick then
		game.log("Your computer is not fast enough to run the server.","error")
		error("Your computer is not fast enough to run the server.")
	else
		socket.sleep(tick-benchmark)			
	end
end
