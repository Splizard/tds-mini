editor = {}


editor.load = function()

end

function editor.savemap(filename)
	file = io.open(filename, "w")
	file:write("Width,Height="..(mapWidth/10)..","..mapHeight/10)
	--[[for id,U in pairs(unitsInGame) do
		file:write("\nRTS.spawn_unit('"..U.id.."',"..U.team..","..U.body:getX()..","..U.body:getY()..","..U.health..","..U.rotate..",{")
		if U.waypoint ~= nil then
			file:write("\n\twaypoints={\n")
			for id,W in pairs(U.waypoint) do
				file:write("\t\t["..id.."]={x="..(W.x or "nil")..",y="..(W.y or "nil")..",target="..(W.target or "nil")..",targetType='"..(W.targetType or "nil").."'},\n")
			end
			file:write("\t},\n")
		end
		file:write("})")
	end]]--
--[[	for id,U in pairs(vehiclesInGame) do
		file:write("\nRTS.spawn_vehicle('"..U.id.."',"..U.team..","..U.body:getX()..","..U.body:getY()..","..U.health..","..U.rotate..",{")
		if U.waypoint ~= nil then
			file:write("\n\twaypoints={\n")
			for id,W in pairs(U.waypoint) do
				file:write("\t\t["..id.."]={x="..(W.x or "nil")..",y="..(W.y or "nil")..",target="..(W.target or "nil")..",targetType='"..(W.targetType or "nil").."'},\n")
			end
			file:write("\t},\n")
		end
		file:write("})")
	end]]--
	for id,U in pairs(objectsInGame) do
		file:write("\nRTS.spawn_object('"..U.id.."',"..U.body:getX()..","..U.body:getY()..")")
	end
	file:close()
end

editor.keypressed = function(key)
	if state == "editor" then
		mouseX, mouseY = camera:mousePosition()
		if key == "p" then
			RTS.spawn_object("spawnpoint", mouseX, mouseY)
		end
		if key == "t" then
			RTS.spawn_object("Tree", mouseX, mouseY)
		end
		if key == "s" then
			editor.savemap("map")
		end
	end
end

editor.update = function()
mouseX, mouseY = camera:mousePosition()

	--Scroll map
	if love.keyboard.isDown("up") or (love.mouse.getY() <= windowHeight-(windowHeight-1)) then
		if camera.y + windowHeight > 0 then camera:move(0,-20) end
	end
	if love.keyboard.isDown("down") or (love.mouse.getY() >= windowHeight-1) then
		if camera.y < mapHeight then camera:move(0,20) end
	end
	if love.keyboard.isDown("left") or (love.mouse.getX() <= windowWidth-(windowWidth-1)) then
		 if camera.x + windowWidth > 0 then camera:move(-20,0) end
	end
	if love.keyboard.isDown("right") or (love.mouse.getX() >= windowWidth-1) then
		if camera.x < mapWidth then camera:move(20,0) end
	end
	
	for id,U in pairs(unitsInGame) do
		if U.dragging == true then
			U.body:setX(mouseX)
			U.body:setY(mouseY)
			return 0
		elseif U.selected == true and (love.keyboard.isDown("rctrl") or love.keyboard.isDown("lctrl")) and love.mouse.isDown( "l" )  then
			U.rotate = math.angle(U.body:getX(),U.body:getY(),mouseX,mouseY)
		end
	end
--	for id,U in pairs(buildingsInGame) do
--		if U.dragging == true then
--			U.body:setX(mouseX)
--			U.body:setY(mouseY)
--			return 0
--		elseif U.selected == true and (love.keyboard.isDown("rctrl") or love.keyboard.isDown("lctrl")) and love.mouse.isDown( "l" )  then
--			U.rotate = math.angle(U.body:getX(),U.body:getY(),mouseX,mouseY)+(math.pi/2)
--		end
--	end
	for id,U in pairs(objectsInGame) do
		if U.dragging == true then
			U.body:setX(mouseX)
			U.body:setY(mouseY)
			return 0
--		elseif U.selected == true and (love.keyboard.isDown("rctrl") or love.keyboard.isDown("lctrl")) and love.mouse.isDown( "l" )  then
--			U.rotate = math.angle(U.body:getX(),U.body:getY(),mouseX,mouseY)+(math.pi/2)
		end
	end
end

--Mouse Dragging handler functions.
function love.mousepressed(x, y, button)
x, y = camera:mousePosition() 
	if state == "editor" then
		if button == "l" then
			for id, U in pairs(unitsInGame) do
				if U.fixture:testPoint( x, y ) then
					U.dragging = true
					U.selected = false

					return 0
				end
			end
			for id, U in pairs(objectsInGame) do
				if U.fixture:testPoint( x, y ) then
					U.dragging = true
					U.selected = false
					return 0
				end
			end
		end
	end
end

function love.mousereleased(x, y, button)
x, y = camera:mousePosition() 
	if state == "editor" then
		if button == "l" then
			for id, U in pairs(unitsInGame) do
				if U.dragging == true then
					U.dragging = false
					U.selected = true
				end
			end
			for id, U in pairs(objectsInGame) do
				if U.dragging == true then
					U.dragging = false
					U.selected = true
				end
			end
			return 0
		end
	end
end
